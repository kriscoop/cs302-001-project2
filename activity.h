/*
Name: Kristian Cooper
Class#: CS-302-001 
Project2
File: activity.h
Description: 	This is the heirarchy header file consisting of base class Activity, and derived classes Tour, Game, and Dine.
		Per program requirements, Tour contains a List slt while game uses an Array slt.
		The base class implements all 16 required operators while the others implement as needed. 
*/

#ifndef ACTIVITY_H
#define ACTIVITY_H

#include <string>
#include <ctime>
#include <cstring>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <list>
#include <array>
#include <vector>

const int MAX_STRING = 256;

////////////// Activity Class (Base) /////////////////////////

class Activity
{
public:
	Activity();
	Activity(const std::string &, const int &);
	Activity(std::istream & input);
	Activity(const Activity & to_copy);
	~Activity();

	std::istream & loadData(std::istream & input);

	friend std::ostream & operator << (std::ostream & output, const Activity & op1);
	friend std::istream & operator >> (std::istream & input, Activity & op2);

	friend bool operator == (const Activity & op1, const Activity & op2);
	friend bool operator == (const Activity & op1, const std::string & op2);
	friend bool operator == (const std::string & op1, const Activity & op2);
	friend bool operator != (const Activity & op1, const Activity & op2);
	friend bool operator != (const Activity & op1, const std::string & op2);
	friend bool operator != (const std::string & op1, const Activity & op2);
	friend bool operator < (const Activity & op1, const Activity & op2);
	friend bool operator < (const Activity & op1, const std::string &);
	friend bool operator < (const std::string & op1, const Activity & op2);
	friend bool operator <= (const Activity & op1, const Activity & op2);
	friend bool operator <= (const Activity & op1, const std::string &);
	friend bool operator <= (const std::string & op1, const Activity & op2);
	friend bool operator > (const Activity & op1, const Activity & op2);
	friend bool operator > (const Activity & op1, const std::string &);
	friend bool operator > (const std::string & op1, const Activity & op2);
	friend bool operator >= (const Activity & op1, const Activity & op2);
	friend bool operator >= (const Activity & op1, const std::string &);
	friend bool operator >= (const std::string & op1, const Activity & op2);

	Activity & operator += (const std::string & op2);
	Activity & operator += (const int & op2);
	Activity & operator -= (const std::string & op2);
	Activity & operator -= (const int & op2);
	Activity & operator = (const Activity & op1);
	friend Activity operator + (const Activity & op1, const std::string & op2);
	friend Activity operator + (const Activity & op1, const int & op2);
	friend Activity operator - (const Activity & op1, const std::string & op2);
	friend Activity operator - (const Activity & op1, const int & op2);

protected:
	int time;
	char * address;
	
	/////////// EXCEPTION HANDLING attempts here /////////
	void copy(char * &, const char *);
	int compare(const char * op1, const char * op2) const;
	void catPlus(char * & op1, const char * & op2);
	void catMinus(char * & op1, const char * & op2);
	void incrementTime(const int & op2);
	std::istream & load(std::istream & input);
};

////////////// Tour Class (Derived) ///////////////////////

class Tour: public Activity
{
public:
	Tour();
	Tour(const std::string &, const int &, const std::string &);
	Tour(std::istream & input);
	Tour(const Tour & to_copy);
	~Tour();

	std::istream & loadData(std::istream & input);

	friend std::ostream & operator << (std::ostream & output, const Tour & op1);
	friend std::istream & operator >> (std::istream & input, Tour & op2);

	friend bool operator == (const Tour & op1, const Tour & op2);
	friend bool operator == (const Tour & op1, const std::string & op2);
	friend bool operator == (const std::string & op1, const Tour & op2);
	friend bool operator != (const Tour & op1, const Tour & op2);
	friend bool operator != (const Tour & op1, const std::string & op2);
	friend bool operator != (const std::string & op1, const Tour & op2);
	friend bool operator < (const Tour & op1, const Tour & op2);
	friend bool operator < (const Tour & op1, const std::string &);
	friend bool operator < (const std::string & op1, const Tour & op2);
	friend bool operator <= (const Tour & op1, const Tour & op2);
	friend bool operator <= (const Tour & op1, const std::string &);
	friend bool operator <= (const std::string & op1, const Tour & op2);
	friend bool operator > (const Tour & op1, const Tour & op2);
	friend bool operator > (const Tour & op1, const std::string &);
	friend bool operator > (const std::string & op1, const Tour & op2);
	friend bool operator >= (const Tour & op1, const Tour & op2);
	friend bool operator >= (const Tour & op1, const std::string &);
	friend bool operator >= (const std::string & op1, const Tour & op2);

	Tour & operator += (const std::string & op2);
	Tour & operator -= (const std::string & op2);
	Tour & operator = (const Tour & op1);
	Tour & operator -- ();

protected:
	char * location;
	std::list<std::string> sites;

	std::istream & load(std::istream & input);
};

////////////// Game Class (Dervied) ///////////////////////

const int MAX_STATE = 26;
const int MAX_TILE = 100;

class Game: public Activity
{
public:
	Game();
	Game(const std::string &, const int &, const std::string &, const int &);
	Game(std::istream & input);
	~Game();

	std::istream & loadData(std::istream & input);

	friend std::ostream & operator << (std::ostream & output, const Game & op1);
	friend std::istream & operator >> (std::istream & input, Game & op2);

	friend bool operator == (const Game & op1, const Game & op2);
	friend bool operator == (const Game & op1, const std::string & op2);
	friend bool operator == (const std::string & op1, const Game & op2);
	friend bool operator != (const Game & op1, const Game & op2);
	friend bool operator != (const Game & op1, const std::string & op2);
	friend bool operator != (const std::string & op1, const Game & op2);
	friend bool operator < (const Game & op1, const Game & op2);
	friend bool operator < (const Game & op1, const std::string & op2);
	friend bool operator < (const std::string & op1, const Game & op2);
	friend bool operator <= (const Game & op1, const Game & op2);
	friend bool operator <= (const Game & op1, const std::string & op2);
	friend bool operator <= (const std::string & op1, const Game & op2);
	friend bool operator > (const Game & op1, const Game & op2);
	friend bool operator > (const Game & op1, const std::string & op2);
	friend bool operator > (const std::string & op1, const Game & op2);
	friend bool operator >= (const Game & op1, const Game & op2);
	friend bool operator >= (const Game & op1, const std::string & op2);
	friend bool operator >= (const std::string & op1, const Game & op2);

	Game & operator += (const int & op1);
	Game & operator -= (const int & op1);
	Game & operator += (const long & op1);
	Game & operator -= (const long & op1);
	Game & operator ++ ();
	Game & operator -- ();

protected:
	std::string saveName;
	long chance;
	int danger;
	int tiles;
	int currPosition;
	int lastPosition;
	int turn;
	int lastRoll;
	int bonus;
	int states;
	std::array<std::string, MAX_STATE> stateDescrip;
	std::array<int, MAX_STATE> stateBonus;	
	std::array<int, MAX_TILE> tileStates;	

	void initializeTiles();
	void increaseTiles(const int &);
	void progress(const int &);
	void checkState();
	std::istream & load(std::istream & input);
};

////////// Dine Implementation ///////////////////////

class Dine: public Activity
{
public:
	Dine();
	Dine(const std::string &, const int &, const std::string &);
	Dine(std::istream & input);
	~Dine();

	std::istream & loadData(std::istream & input);

	friend std::ostream & operator << (std::ostream & output, const Dine & op1);
	friend std::istream & operator >> (std::istream & input, Dine & op2);

	friend bool operator == (const Dine & op1, const Dine & op2);
	friend bool operator == (const Dine & op1, const std::string & op2);
	friend bool operator == (const std::string & op1, const Dine & op2);
	friend bool operator != (const Dine & op1, const Dine & op2);
	friend bool operator != (const Dine & op1, const std::string & op2);
	friend bool operator != (const std::string & op1, const Dine & op2);
	friend bool operator < (const Dine & op1, const Dine & op2);
	friend bool operator < (const Dine & op1, const std::string & op2);
	friend bool operator < (const std::string & op1, const Dine & op2);
	friend bool operator <= (const Dine & op1, const Dine & op2);
	friend bool operator <= (const Dine & op1, const std::string & op2);
	friend bool operator <= (const std::string & op1, const Dine & op2);
	friend bool operator > (const Dine & op1, const Dine & op2);
	friend bool operator > (const Dine & op1, const std::string & op2);
	friend bool operator > (const std::string & op1, const Dine & op2);
	friend bool operator >= (const Dine & op1, const Dine & op2);
	friend bool operator >= (const Dine & op1, const std::string & op2);
	friend bool operator >= (const std::string & op1, const Dine & op2);

	friend double operator + (const Dine & op1, const int & op2);
	friend double operator - (const Dine & op1, const int & op2);
	Dine & operator += (const double & op1);
	Dine & operator -= (const double & op1);
	Dine & operator += (const std::string & op1);
	Dine & operator -= (const std::string & op1);
	Dine & operator -- ();

protected:
	std::string name;
	std::vector<std::string> menu;
	std::vector<double> prices;
	
	std::istream & load(std::istream & input);
};

#endif




