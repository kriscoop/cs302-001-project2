#ifndef DLL_TPP
#define DLL_TPP

#include <iostream>

///////////// Node Implementation ///////////////

template <class TYPE>
Node<TYPE> :: Node(): next(nullptr), prev(nullptr)
{
}

template <class TYPE>
Node<TYPE> :: Node(std::istream & input): next(nullptr), prev(nullptr)
{
	load(input);
}

template <class TYPE>
Node<TYPE> :: Node(TYPE & someData): next(nullptr), prev(nullptr), data(someData)
{
}

template <class TYPE>
Node<TYPE> :: Node(const Node<TYPE> & aNode): next(nullptr), prev(nullptr)
{
	data = aNode.data;
}

template <class TYPE>
Node<TYPE> :: ~Node()
{
}

template <class TYPE>
std::istream & Node<TYPE> :: loadData(std::istream & input)
{
	return data.loadData(input);
}

template <class TYPE>
Node<TYPE> * & Node<TYPE> :: getNext()
{
	return next;
}

template <class TYPE>
Node<TYPE> * & Node<TYPE> :: getPrev()
{
	return prev;
}

template <class TYPE>
void Node<TYPE> :: setNext(Node<TYPE> * & node)
{
	next = node;
}

template <class TYPE>
void Node<TYPE> :: setPrev(Node<TYPE> * & node)
{
	prev = node;
}

template <class TYPE>
Node<TYPE> & Node<TYPE> :: operator = (const Node<TYPE> & op1)
{
	next = nullptr;
	prev = nullptr;
	data = op1.data;
	return *this;
}

template <class TYPE>
Node<TYPE> & Node<TYPE> :: operator = (const TYPE & op1)
{
	data = op1;
	return *this;
}

template <class TYPE>
//////////// Comparison for sorting ///////////
bool Node<TYPE> :: operator < (const Node<TYPE> & op1)
{
	return data < op1.data;
}

template <class TYPE>
std::ostream & operator << (std::ostream & output, const Node<TYPE> & node)
{
	output << node.data;
	return output;
}

template <class TYPE>
//////////////// Extract data copy ////////////////
TYPE & operator << (TYPE & op1, const Node<TYPE> & node)
{
	op1 = node.data;
	return op1;
}

template <class TYPE>
////////////// load from file /////////////////
std::istream & Node<TYPE> :: load(std::istream & input)
{
	if(input)
	{
		TYPE data{input};
		this->data = data;
	}
		return input;
}

/////////// DLL Implementation ///////////////////////

template <class TYPE>
DLL<TYPE> :: DLL(): head(nullptr), current(nullptr)
{
}

template <class TYPE>
DLL<TYPE> :: DLL(std::istream & input): head(nullptr), current(nullptr)
{
	load(input);
}

template <class TYPE>
DLL<TYPE> :: DLL(const Node<TYPE> & node): head(nullptr), current(nullptr)
{
	Node<TYPE> * newNode = new Node<TYPE>(node);
	head = newNode;
	current = newNode;
}

template <class TYPE>
DLL<TYPE> :: DLL(Node<TYPE> * & node): head(nullptr)
{
	head = node;
	current = node;
	node = nullptr;
}

template <class TYPE>
DLL<TYPE> :: DLL(const DLL<TYPE> & aDLL): head(nullptr)
{
	*this = aDLL;
}

template <class TYPE>
DLL<TYPE> :: ~DLL()
{
	if(head)
		destroy(head);
}

template <class TYPE>
void DLL<TYPE> :: insert(Node<TYPE> * & aNode)
{
	if(head)
	{
		if(*aNode < *head)
		{

			aNode->setNext(head);
			head->setPrev(aNode);
			head = aNode;
			current = head;
			aNode = nullptr;
		}
		else
		{
			if(!head->getNext())
				aNode->setPrev(head);
			insert(head->getNext(), aNode);		//recursive call here
		}
	}
	else
	{
		head = aNode;
		current = head;
		aNode = nullptr;
	}
}

template <class TYPE>
int DLL<TYPE> :: retrieve(const int & index, Node<TYPE> * & aNode)
{
	aNode = nullptr;
	if(head)
	{
		if(index - 1 == 0)
			aNode = head;
		else
			return retrieve(head->getNext(), index, aNode);	//recursive call here
		return 1;
	}
	return 0;
}

template <class TYPE>
int DLL<TYPE> :: remove(const int & index)
{
	if(head)
	{
		if(index - 1 == 0)
			removeNode(head);
		else
			return remove(head->getNext(), index);		//recursive call here
		return 1;
	}
	return 0;
}

template <class TYPE>
int DLL<TYPE> :: removeAll()
{
	if(head)
	{
		destroy(head);
		head = nullptr;
		return 1;
	}
	return 0;
}

template <class TYPE>
int DLL<TYPE> :: display()
{
	if(head)
	{
		std::cout << *this;
		return 1;
	}
	return 0;
}

template <class TYPE>
std::istream & DLL<TYPE> :: loadData(std::istream & input)
{
	return load(input);
}

template <class TYPE>
////////////// Display ///////////////
std::ostream & operator << (std::ostream & output, DLL<TYPE> & op1)
{
	if(op1.head)
	{
		if(op1.current)
			output << "Current Pointer:" << std::endl << *op1.current << std::endl; 
		else
			output << "Current Pointer:\nNull"; 

		return op1.print(op1.head, output);
	}
	else
		return output;
}

template <class TYPE>
////////////// Set current to current + num ///////////////
DLL<TYPE> & DLL<TYPE> :: operator += (const int & op2)
{
	Node<TYPE> * temp = increment(op2);
	if(temp)
		current = temp;
	else if(op2 < 0)
		current = head;
	else if(op2 > 0)
		moveToEnd(current);
	return *this;
}

template <class TYPE>
////////////// Set current to current - num ///////////////
DLL<TYPE> & DLL<TYPE> :: operator -= (const int & op2)
{
	int num = op2 * -1;
	Node<TYPE> * temp = increment(num);
	if(temp)
		current = temp;
	else if(op2 > 0)
		current = head;
	else if(op2 < 0)
		moveToEnd(current);
	return *this;
}

template <class TYPE>
/////////////////// Set current to last node //////////////// 
DLL<TYPE> & DLL<TYPE> :: operator ++ ()
{
	moveToEnd(head);
	return *this;
}

template <class TYPE>
/////////////////// Set current to head //////////////// 
DLL<TYPE> & DLL<TYPE> :: operator -- ()
{
	current = head;
	return *this;
}

template <class TYPE>
/////////////////// Peek from current + number //////////////// 
Node<TYPE> * operator + (DLL<TYPE> & op1, const int & op2)
{
	return op1.increment(op2); 
}

template <class TYPE>
/////////////////// Peek from current - number //////////////// 
Node<TYPE> * operator - (DLL<TYPE> & op1, const int & op2)
{
	int num = op2 * -1;
	return op1.increment(num); 
}

template <class TYPE>
DLL<TYPE> & DLL<TYPE> :: operator = (const DLL<TYPE> & op1)
{
	if(this == &op1)
		return *this;

	if(op1.head)
	{
		if(head)
			destroy(head);
		Node<TYPE> * newNode = new Node<TYPE>(*op1.head);
		head = newNode;
		copyNode(op1.head->getNext(), head);	// recursive copy function
		this->current = this->head;
	}
	else
		if(head)
			destroy(head);			// recursive destroy
	return *this;
}

template <class TYPE>
//////////////////// Recursive Destroy ///////////////////
void DLL<TYPE> :: destroy(Node<TYPE> * & current)
{
	if(current)
	{
		destroy(current->getNext());
		delete current;
	}
}

template <class TYPE>
///////////// HElper function to insert ///////////////
void DLL<TYPE> :: insert(Node<TYPE> * & current, Node<TYPE> * & aNode)
{
	if(current)
	{
		if(*aNode < *current)
		{
			Node<TYPE> * prev = current->getPrev();
			aNode->setNext(current);
			aNode->setPrev(prev);
			current->setPrev(aNode);
			prev->setNext(aNode);
			aNode = nullptr;
		}
		else
		{
			if(!current->getNext())
				aNode->setPrev(current);
			insert(current->getNext(), aNode);
		}
	}
	else
	{
		current = aNode;
		aNode = nullptr;
	}
}

template <class TYPE>
////////////// Recursive retrieve function ///////////////////
int DLL<TYPE> :: retrieve(Node<TYPE> * & current, const int & index, Node<TYPE> * & aNode)
{
	static int count = 1;
	Node<TYPE>* temp = current;
	--*this;

	if(current)
	{
		if(index - 1 == count)
		{
			aNode = current;
			count = 1;
			return count;
		}
		else
		{
			count++;
			return retrieve(current->getNext(), index, aNode);
		}
	}
	else
		count = 1;

	current = temp;
	return 0;
}

template <class TYPE>
////////////// Recursive removal function ///////////////////
int DLL<TYPE> :: remove(Node<TYPE> * & current, const int & index)
{
	static int count = 1;

	if(current)
	{
		if(index - 1 == count)
		{
			removeNode(current);
			count = 1;
			return count;
		}
		else
		{
			count++;
			return remove(current->getNext(), index);
		}
	}
	else
		count = 1;

	return 0;
}

template <class TYPE>
////////////// Remove Node helper ///////////////
void DLL<TYPE> :: removeNode(Node<TYPE> * & current)
{
	if(current)
	{
		if(current == this->current)
		{
			if(current->getNext())
				this->current = current->getNext();
			else if(current->getPrev())
				this->current = current->getPrev();
			else
				this->current = nullptr;
		}

		Node<TYPE> * deletion = current;
		Node<TYPE> * next = current->getNext();

		current = next;
		delete deletion;
	}
}

template <class TYPE>
/////////////// Print ostream helper function /////////////////
std::ostream & DLL<TYPE> :: print(Node<TYPE> * & current, std::ostream & output)
{
	static int count = 0;
	if(current)
	{
		count++;
		output << std::endl << count << "." <<  std::endl << *current << std::endl; 
		return print(current->getNext(), output);
	}
	else
	{
		count = 0;
		return output;
	}
		// Exception handling here? I don't think this is actually necessary.
}

template <class TYPE>
///////////// Copy list assignment operator helper /////////////
void DLL<TYPE> :: copyNode(Node<TYPE> * & source, Node<TYPE> * & copy)
{
	if(source)
	{
		Node<TYPE> * newNode = new Node<TYPE>(*source);
		newNode->setPrev(copy);
		copy->setNext(newNode);
		copyNode(source->getNext(), newNode);
	}
}

template <class TYPE>
//////////// Increment current operator /////////////////
Node<TYPE> * & DLL<TYPE> :: increment(const int & op1)
{
	if(current)
	{
		return increment(current, op1);
	}
		return current;
}

template <class TYPE>
/////////// Increment recursive helper ///////////////
Node<TYPE> * & DLL<TYPE> :: increment(Node<TYPE> * & op1, const int & op2)
{
	static int count = 0;

	if(op1)
	{
		if(op2 <= 0)
		{
			if(op2 == count)
			{	
				count = 0;
				return op1;
			}
			else
			{
				count--;
				return increment(op1->getPrev(), op2);
			}
		}
		else
		{
			if(op2 == count)
			{
				count = 0;
				return op1;
			}
			else
			{
				count++;
				return increment(op1->getNext(), op2);
			}
		}
	}
	else
	{
		count = 0;
		return op1;
	}
}

template <class TYPE>
//////////////// move current to end ///////////////
void DLL<TYPE> :: moveToEnd(Node<TYPE> * & current)
{
	if(current)
	{
		if(current->getNext())
			moveToEnd(current->getNext());
		else
			this->current = current;
	}
}

template <class TYPE>
//////////// load from file ///////////////////
std::istream & DLL<TYPE> :: load(std::istream & input)
{
	if(input)
	{
		Node<TYPE> * newNode;
		newNode = new Node<TYPE>{input};

		if(!input.eof())
			removeAll();

		while(!input.eof())
		{
			insert(newNode);
			if(input.peek(), !input.eof())
				newNode = new Node<TYPE>{input};
		}
	}

	return input;
}



#endif

