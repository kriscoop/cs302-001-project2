CC = g++
CPPFLAGS = -g -Wall -std=c++17

project2:	cooper-kristian-Project2.o activity.o
	g++ cooper-kristian-Project2.o activity.o -o project2

cooper-kristian-Project2.o:	cooper-kristian-Project2.cpp
	$(CC) $(CPPFLAGS) -c cooper-kristian-Project2.cpp 

activity.o:	activity.cpp
	$(CC) $(CPPFLAGS) -c activity.cpp

clean:
	$(info -- cleaning the direcotry --)
	rm *.o
	rm project2
