#include <iostream>
#include "menu.h"
/*
template<class TYPE> void isEqual(TYPE op1, TYPE op2);
template<class TYPEA, class TYPEB> void isEqual(TYPEA op1, TYPEB op2);
template<class TYPE> void isNotEqual(TYPE op1, TYPE op2);
template<class TYPEA, class TYPEB> void isNotEqual(TYPEA op1, TYPEB op2);
template<class TYPE> void lessThan(TYPE op1, TYPE op2);
template<class TYPEA, class TYPEB> void lessThan(TYPEA op1, TYPEB op2);
template<class TYPE> void lessThanEqual(TYPE op1, TYPE op2);
template<class TYPEA, class TYPEB> void lessThanEqual(TYPEA op1, TYPEB op2);
template<class TYPE> void greatThan(TYPE op1, TYPE op2);
template<class TYPEA, class TYPEB> void greatThan(TYPEA op1, TYPEB op2);
template<class TYPE> void greatThanEqual(TYPE op1, TYPE op2);
template<class TYPEA, class TYPEB> void greatThanEqual(TYPEA op1, TYPEB op2);
*/

using namespace std;

int main()
{
	//Driver test of activity class
	
	ifstream inputT, inputG, inputD;
	inputT.open("tourData.txt");
	inputG.open("gameData.txt");
	inputD.open("dineData.txt");
	
	if(inputT && inputG && inputD)
	{
		Menu menu(inputT, inputG, inputD);
		menu.run();
	}
	else
		cout << "Error loading from file" << endl;
/*
	DLL<Tour> listT;
	DLL<Game> listG;
	DLL<Dine> listD;

	if(inputT)
		listT.loadData(inputT);
	if(inputG)
		listG.loadData(inputG);
	if(inputD)
		listD.loadData(inputD);
		
//	cout << listT << endl;
	int num = 10;
	cout << "Removing " << num  << endl;
	listT.remove(num);
	cout << listT << endl;
	num = 3;
	cout << "Removing " << num  << endl;
	listT.remove(num);
	
	cout << listT << endl;
	listT.removeAll();

	cout << listT << endl;

	num = 1;
	Node<Game> * temp = nullptr;
	Game test;
	cout << "Printing Game" << endl << listG << endl;
	listG.retrieve(num,temp) ;
	test << *temp;
	++test;
	*temp = test;
	cout << "Playing Game" << endl << listG << endl;
	cout << listD << endl;
*/


/*
	///////// UNCOMMENT to test Dine /////////////
	Dine testA{"Cherry Lane", 7, "Burger King"};

	double price;
	string buffer;

	cout << testA << endl;
	cout << "testing the >> operator" << endl << "Enter the name: ";
	cin >> testA;
	cout << "Testing += function x4" << endl;
	
	for(int i = 0; i < 4; i++)
	{
		cout << "Enter item name: ";
		getline(cin, buffer);
		cout << "Enter price: ";
		cin >> price;
		cin.ignore(1, '\n');
		testA += buffer;
		testA += price;
	}

	cout << testA << endl;
	cout << "testing -= (string) and -= (int)" << endl;
	cout << "Enter item name: ";
	getline(cin, buffer);
	cout << "Enter price: ";
	cin >> price;
	cin.ignore(1, '\n');
	testA -= buffer;
	testA -= price;

	cout << testA << endl;

	cout << "testing -- function" << endl;
	--testA;
	cout << testA << endl;
	
	cout << "totaling up all 3 menu items with +" << endl;
	double total = 0;
	
	for(int i = 1; i<=3; i++)
		total += (testA + i);
	
	cout << testA << endl << "Total of the items is: $" << total << endl;
	cout << "removing price of item 2 from item with -" << endl;
	total -= (testA - 2);
	cout << "New total after removing item 2 " << total << endl;
*/

/*
	////////////// Do not uncomment ///////////
	All operators for the Dine function

	friend std::ostream & operator << (std::ostream & output, const Dine & op1);
	friend std::istream & operator >> (std::istream & input, Dine & op2);

	friend bool operator == (const Dine & op1, const Dine & op2);
	friend bool operator == (const Dine & op1, const std::string & op2);
	friend bool operator == (const std::string & op1, const Dine & op2);
	friend bool operator != (const Dine & op1, const Dine & op2);
	friend bool operator != (const Dine & op1, const std::string & op2);
	friend bool operator != (const std::string & op1, const Dine & op2);
	friend bool operator < (const Dine & op1, const Dine & op2);
	friend bool operator < (const Dine & op1, const std::string & op2);
	friend bool operator < (const std::string & op1, const Dine & op2);
	friend bool operator <= (const Dine & op1, const Dine & op2);
	friend bool operator <= (const Dine & op1, const std::string & op2);
	friend bool operator <= (const std::string & op1, const Dine & op2);
	friend bool operator > (const Dine & op1, const Dine & op2);
	friend bool operator > (const Dine & op1, const std::string & op2);
	friend bool operator > (const std::string & op1, const Dine & op2);
	friend bool operator >= (const Dine & op1, const Dine & op2);
	friend bool operator >= (const Dine & op1, const std::string & op2);
	friend bool operator >= (const std::string & op1, const Dine & op2);

	friend double operator + (const Dine & op1, const int & op2);
	friend double operator - (const Dine & op1, const int & op2);
	Dine & operator += (const double & op1);
	Dine & operator -= (const double & op1);
	Dine & operator += (const std::string & op1);
	Dine & operator -= (const std::string & op1);
	Dine & operator -- ();
*/

/*
	///////// UNCOMMENT TO TEST Game ?///////////////

	Game testA {"6th Ave", 6, "First Save", 95};
	cout << testA << endl;
	cout << "Override filename with (>>): ";
	cin >> testA;
	
	cout << "Testing the roll (++) function" << endl;
	++testA;
	
	cout << testA << endl;
	testA += 10;
	
	cout << "Testing the increase tiles (+= 10 (int)) function" << endl;

	cout << testA << endl << "Rolling 3 times and testing (=) and copy constructor functions" << endl << endl;
	++testA;
	++testA;
	++testA;
	Game testB{testA};
	Game testC = testB;

	cout << "printing testB (copy constructor)" << endl << testB << endl << endl;
	cout << "printing testC (=)" << endl << testC << endl << endl;

	cout << "testing increase danger function (-= 5), increase chance (+= 10 (long)), and rolling 3 times" << endl;
	long chance = 10;
	testC -= 5;
	testC += chance;
	++testC;
	++testC;
	++testC;
	
	cout << testC << endl;

	cout << "Resetting testC (--)" << endl;
	--testC;
	cout << testC << endl << endl;
	
	cout << "Testing the increase tiles (+= 10 (int)) function" << endl;
	testA += 10;

	cout << testA << endl;
*/
	
/*	
	/////////////// Do not Uncomment /////////////
	All operators to test the game function. 

	friend bool operator == (const Game & op1, const Game & op2);
	friend bool operator == (const Game & op1, const std::string & op2);
	friend bool operator == (const std::string & op1, const Game & op2);
	friend bool operator != (const Game & op1, const Game & op2);
	friend bool operator != (const Game & op1, const std::string & op2);
	friend bool operator != (const std::string & op1, const Game & op2);
	friend bool operator < (const Game & op1, const Game & op2);
	friend bool operator < (const Game & op1, const std::string & op2);
	friend bool operator < (const std::string & op1, const Game & op2);
	friend bool operator <= (const Game & op1, const Game & op2);
	friend bool operator <= (const Game & op1, const std::string & op2);
	friend bool operator <= (const std::string & op1, const Game & op2);
	friend bool operator > (const Game & op1, const Game & op2);
	friend bool operator > (const Game & op1, const std::string & op2);
	friend bool operator > (const std::string & op1, const Game & op2);
	friend bool operator >= (const Game & op1, const Game & op2);
	friend bool operator >= (const Game & op1, const std::string & op2);
	friend bool operator >= (const std::string & op1, const Game & op2);

	Game & operator += (const int & op1);
	Game & operator -= (const int & op1);
	Game & operator += (const long & op1);
	Game & operator -= (const long & op1);
	Game & operator ++ ();
	Game & operator -- ();
*/

/*
	///////////// UNCOMMENT TO TEST Tour //////////////
	Tour testA;
	cout << "Enter the location: ";
	cin >> testA;

	cout << "Enter the address: ";
	cin >> static_cast<Activity&>(testA);
	
	testA.Activity::operator+=(3);
	cout << testA << endl;

	string buffer;
	for(int i = 0; i<4; i++)
	{
		cout << "Enter attraction: ";
		getline(cin, buffer);
		cout << "adding string using += " << buffer << endl; 
		testA += buffer;
	}

	cout << "Printing the tour" << endl << testA << endl << "Removing some attractions with --" << endl;
	--testA;
	--testA;
	cout << "Printing after deletions." << endl << testA << endl;

	cout << "Enter attraction: ";
	getline(cin, buffer);
	cout << "replace previous with string using -= " << buffer << endl;
	testA -= buffer;
	cout << "Printing after replacement" << endl << testA << endl;
*/


/*
	/////////////// Do not Uncomment //////////////////
	Operators Available to Tour class

	friend std::ostream & operator << (std::ostream & output, const Tour & op1);
	friend std::istream & operator >> (std::istream & input, Tour & op2);

	friend bool operator == (const Tour & op1, const Tour & op2);
	friend bool operator == (const Tour & op1, const std::string & op2);
	friend bool operator == (const std::string & op1, const Tour & op2);
	friend bool operator != (const Tour & op1, const Tour & op2);
	friend bool operator != (const Tour & op1, const std::string & op2);
	friend bool operator != (const std::string & op1, const Tour & op2);
	friend bool operator < (const Tour & op1, const Tour & op2);
	friend bool operator < (const Tour & op1, const std::string &);
	friend bool operator < (const std::string & op1, const Tour & op2);
	friend bool operator <= (const Tour & op1, const Tour & op2);
	friend bool operator <= (const Tour & op1, const std::string &);
	friend bool operator <= (const std::string & op1, const Tour & op2);
	friend bool operator > (const Tour & op1, const Tour & op2);
	friend bool operator > (const Tour & op1, const std::string &);
	friend bool operator > (const std::string & op1, const Tour & op2);
	friend bool operator >= (const Tour & op1, const Tour & op2);
	friend bool operator >= (const Tour & op1, const std::string &);
	friend bool operator >= (const std::string & op1, const Tour & op2);

	Tour & operator += (const std::string & op2);
	Tour & operator -= (const std::string & op2);
	Tour & operator = (const Tour & op1);
	Tour & operator -- ();
*/

/*
	int test = 10;
	Node<int> * buffer = new Node<int>(test);
	
	DLL<int> list(buffer);

	int pow = 0;
	for(int i = 0; i < 5; i++)
	{
		pow = i * i;
		buffer = new Node<int>(pow);
		list.insert(buffer);
	}

	pow = 3;
	list.display();
	cout << "printing the list after deleting " << pow << endl;
	list.remove(pow);
	cout << list;
	cout << "selecting index 2" << endl;
	int num = 2-1;
	buffer = list + num;
	cout << *buffer << endl << "setting current to 4 and printing" << endl;
	--list;
	num = 4-1;
	list += num;
	buffer = list + 0;
	cout << *buffer << endl;
	
	cout << "Deleting all in list" << endl;
	list.removeAll();
*/

/*
	///////////// Test the activity class ///////////////// UNCOMMENT code after main to test
	cout << "testing: ostream << Activity" << endl;
	string testA = "1234 Cherry Lane, Portland OR 97123", testB = "No match string";
	Activity eventA(testA, 12);
	cout << "Event A: " << eventA << endl;
	
	Activity eventB(eventA), eventC(eventA);
	cout << "testing: copy const" << endl;
	cout << "Event B: " << eventB << endl;
	
	cout << "testing: ostream >> Activity" << endl;
	cout << "Enter the new address for Event B: ";
	cin >> eventB;
	cout << "Event B: " << eventB << endl;

	cout << "testing: Activity == Activity" << endl;
	isEqual(eventA, eventB);
	isEqual(eventA, eventC);
	
	
	cout << "testing: Activity == string" << endl;
	isEqual(eventA, testB);
	isEqual(eventA, testA);
	isEqual(testB, eventA);
	isEqual(testA, eventA);
	cout << endl;
	
	cout << "testing: Activity != Activity" << endl;
	isNotEqual(eventA, eventB);
	isNotEqual(eventA, eventC);

	cout << "testing: Activity != string" << endl;
	isNotEqual(eventA, testB);
	isNotEqual(eventA, testA);
	isNotEqual(testB, eventA);
	isNotEqual(testA, eventA);
	cout << endl;
	
	cout << "testing: Activity < Activity" << endl;
	lessThan(eventA, eventB);
	lessThan(eventA, eventC);

	cout << "testing: Activity < string" << endl;
	lessThan(eventA, testB);
	lessThan(eventA, testA);
	lessThan(testB, eventA);
	lessThan(testA, eventA);
	cout << endl;
	
	cout << "testing: Activity <= Activity" << endl;
	lessThanEqual(eventA, eventB);
	lessThanEqual(eventA, eventC);

	cout << "testing: Activity <= string" << endl;
	lessThanEqual(eventA, testB);
	lessThanEqual(eventA, testA);
	lessThanEqual(testB, eventA);
	lessThanEqual(testA, eventA);
	cout << endl;
	
	cout << "testing: Activity > Activity" << endl;
	greatThan(eventA, eventB);
	greatThan(eventA, eventC);

	cout << "testing: Activity > string" << endl;
	greatThan(eventA, testB);
	greatThan(eventA, testA);
	greatThan(testB, eventA);
	greatThan(testA, eventA);
	cout << endl;
	
	cout << "testing: Activity >= Activity" << endl;
	greatThanEqual(eventA, eventB);
	greatThanEqual(eventA, eventC);

	cout << "testing: Activity >= string" << endl;
	greatThanEqual(eventA, testB);
	greatThanEqual(eventA, testA);
	greatThanEqual(testB, eventA);
	greatThanEqual(testA, eventA);
	cout << endl;
	
	string testC = "... string added";
	cout << "testing: Activity += string" << endl;
	cout << "before += " << testC << ": " << eventA << endl;
	eventA += testC;
	cout << "after += " << testC << ": " << eventA << endl;
	cout << endl;

	cout << "testing: Activity += int" << endl;
	cout << "before += 3: " << eventA << endl;
	eventA += 3;
	cout << "after += 3: " << eventA << endl;
	cout << endl;

	string testD = "... remove and append";
	cout << "testing: Activity -= string" << endl;
	cout << "before -= " << testD << ": " << eventA << endl;
	eventA += testD;
	cout << "after += " << testD << ": " << eventA << endl;
	cout << endl;

	cout << "testing: Activity -= int" << endl;
	cout << "before -= 10: " << eventA << endl;
	eventA -= 10;
	cout << "after -= 10: " << eventA << endl;
	cout << endl;
	
	Activity eventD;	
	cout << "testing: Activity + string" << endl;
	eventD = (eventB + testD);
	cout << eventB << " + " << testD << " = " << eventD;
	cout << endl;
	
	cout << "testing: Activity + int" << endl;
	eventD = eventC + 8;
	cout << eventC << " + 8 = " << eventD;
	cout << endl;
	
	cout << "testing: Activity - string" << endl;
	eventD = eventB - testC;
	cout << eventB << " - " << testC << " = " << eventD;
	cout << endl;
	
	cout << "testing: Activity - int" << endl;
	eventD = eventC - 25;
	cout << eventC << " - 25 = " << eventD;
	cout << endl;
*/
	

/*	Driver should test all available overloads for Activity class. //////KEEP COMMENTED/////

        friend ostream & operator << (ostream & output, const Activity & op1);
        friend istream & operator >> (istream & input, Activity & op2);

        friend bool operator == (const Activity & op1, const Activity & op2);
        friend bool operator == (const Activity & op1, const std::string & op2);
        friend bool operator == (const std::string & op1, const Activity & op2);
        friend bool operator != (const Activity & op1, const Activity & op2);
        friend bool operator != (const Activity & op1, const std::string & op2);
        friend bool operator != (const std::string & op1, const Activity & op2);
        friend bool operator < (const Activity & op1, const Activity & op2);
        friend bool operator < (const Activity & op1, const std::string &);
        friend bool operator < (const std::string & op1, const Activity & op2);
        friend bool operator <= (const Activity & op1, const Activity & op2);
        friend bool operator <= (const Activity & op1, const std::string &);
        friend bool operator <= (const std::string & op1, const Activity & op2);
        friend bool operator > (const Activity & op1, const Activity & op2);
        friend bool operator > (const Activity & op1, const std::string &);
        friend bool operator > (const std::string & op1, const Activity & op2);
        friend bool operator >= (const Activity & op1, const Activity & op2);
        friend bool operator >= (const Activity & op1, const std::string &);
        friend bool operator >= (const std::string & op1, const Activity & op2);

	Activity & operator += (const std::string & op2);
	Activity & operator += (const int & op2);
	Activity & operator -= (const std::string & op2);
	Activity & operator -= (const int & op2);
	Activity operator = (const Activity & op1);
	Activity operator + (const std::string & op1);
	Activity operator + (const int & op1);
	Activity operator - (const std::string & op1);
	Activity operator - (const int & op1);
*/
	return 0;
}

/*

	///// Uncomment to test Activity function;

template <class TYPE> void isEqual(TYPE op1, TYPE op2)
{
	bool test = op1 == op2;
	if(test)
		cout << "True, " << op1 << " == " << op2 << endl;
	else
		cout << "False, " << op1 << " != " << op2 << endl;
}

template <class TYPEA, class TYPEB> void isEqual(TYPEA op1, TYPEB op2)
{
	bool test = op1 == op2;
	if(test)
		cout << "True, " << op1 << " == " << op2 << endl;
	else
		cout << "False, " << op1 << " != " << op2 << endl;
}

template <class TYPE> void isNotEqual(TYPE op1, TYPE op2)
{
	bool test = op1 != op2;
	if(test)
		cout << "True, " << op1 << " != " << op2 << endl;
	else
		cout << "False, " << op1 << " == " << op2 << endl;
}

template <class TYPEA, class TYPEB> void isNotEqual(TYPEA op1, TYPEB op2)
{
	bool test = op1 != op2;
	if(test)
		cout << "True, " << op1 << " != " << op2 << endl;
	else
		cout << "False, " << op1 << " == " << op2 << endl;
}

template <class TYPE> void lessThan(TYPE op1, TYPE op2)
{
	bool test = op1 < op2;
	if(test)
		cout << "True, " << op1 << " < " << op2 << endl;
	else
		cout << "False, " << op1 << " !< " << op2 << endl;
}

template <class TYPEA, class TYPEB> void lessThan(TYPEA op1, TYPEB op2)
{
	bool test = op1 < op2;
	if(test)
		cout << "True, " << op1 << " < " << op2 << endl;
	else
		cout << "False, " << op1 << " !< " << op2 << endl;
}

template <class TYPE> void lessThanEqual(TYPE op1, TYPE op2)
{
	bool test = op1 < op2;
	if(test)
		cout << "True, " << op1 << " <= " << op2 << endl;
	else
		cout << "False, " << op1 << " !<= " << op2 << endl;
}

template <class TYPEA, class TYPEB> void lessThanEqual(TYPEA op1, TYPEB op2)
{
	bool test = op1 < op2;
	if(test)
		cout << "True, " << op1 << " <= " << op2 << endl;
	else
		cout << "False, " << op1 << " !<= " << op2 << endl;
}

template <class TYPE> void greatThan(TYPE op1, TYPE op2)
{
	bool test = op1 < op2;
	if(test)
		cout << "True, " << op1 << " < " << op2 << endl;
	else
		cout << "False, " << op1 << " !< " << op2 << endl;
}

template <class TYPEA, class TYPEB> void greatThan(TYPEA op1, TYPEB op2)
{
	bool test = op1 < op2;
	if(test)
		cout << "True, " << op1 << " < " << op2 << endl;
	else
		cout << "False, " << op1 << " !< " << op2 << endl;
}

template <class TYPE> void greatThanEqual(TYPE op1, TYPE op2)
{
	bool test = op1 < op2;
	if(test)
		cout << "True, " << op1 << " <= " << op2 << endl;
	else
		cout << "False, " << op1 << " !<= " << op2 << endl;
}

template <class TYPEA, class TYPEB> void greatThanEqual(TYPEA op1, TYPEB op2)
{
	bool test = op1 < op2;
	if(test)
		cout << "True, " << op1 << " <= " << op2 << endl;
	else
		cout << "False, " << op1 << " !<= " << op2 << endl;
}

*/
