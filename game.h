#ifndef GAME_H
#define GAME_H


class Game:
{
public:
        friend ostream & operator << (ostream & output, const Game & op1);
        friend istream & operator >> (istream & input, Game & op2);

        friend bool operator == (const Game & op1, const Game & op2);
        friend bool operator == (const Game & op1, const string &);
        friend bool operator == (const string & op1, const Game & op2);
        friend bool operator != (const Game & op1, const Game & op2);
        friend bool operator != (const Game & op1, const string &);
        friend bool operator != (const string & op1, const Game & op2);

	Game & operator += (Game & op1, const string & op2);
	Game operator = (const Game & op1);

	Game operator < (
	Game operator <= (
	Game operator > (
	Game operator >= (
	
	Game operator += (
	Game operator -= (
	Game operator + (
	Game operator - (

protected:
	int score;
	string title;
	
};

#endif
