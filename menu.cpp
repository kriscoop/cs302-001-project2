#include "menu.h"

Menu :: Menu()
{
}

Menu :: Menu(istream & tourF, istream & gameF, istream & dineF)
{
	loadTours(tourF);
	loadGames(gameF);
	loadDines(dineF);
}

Menu :: ~Menu()
{
}

int Menu :: loadTours(istream & input)
{
	if(input)
	{
		DLL<Tour> temp(input);
		tours = temp;
		return 1;
	}
	return 0;
}

int Menu :: loadGames(istream & input)
{
	if(input)
	{
		DLL<Game> temp(input);
		games = temp;
		return 1;
	}
	return 0;
}

int Menu :: loadDines(istream & input)
{
	if(input)
	{
		DLL<Dine> temp(input);
		dines = temp;
		return 1;
	}
	return 0;
}


int Menu :: run()
{
	char option;
	topMenuSelect(option);
	return 0;
}


///////////// TOP MENU ////////////////

int Menu :: printTopMenu() const
{
	cout << endl;
	cout << "Top Menu:" << endl;
	cout << "A) Tour Menu" << endl;
	cout << "B) Game Menu" << endl;
	cout << "C) Dine Menu" << endl;
	cout << "Q) Quit" << endl;
	cout << "Selection: ";
	
	return 1;
}

int Menu :: topMenuSelect(char & option)
{
	do
	{
		printTopMenu();
		option = getChar();
		topMenuRead(option);
	}
	while(option != 'q');
	return 1;
}

int Menu :: topMenuRead(char & option)
{
	switch(option)
	{
		case 'a':
			listMenuSelect(tours, option);
			break;
		case 'b':
			listMenuSelect(games, option);
			break;
		case 'c':
			listMenuSelect(dines, option);
			break;
		case 'q':
			break;
		default:
			cout << "Invalid option..." << endl;
			break;
	}	
	return 1;
}

/////////// TOUR MENU ///////////////////

void Menu :: printTourMenu() const
{
	cout << endl;
	cout << "Tour Menu:" << endl;
	cout << "A) Display Tours" << endl;
	cout << "B) Insert Tour" << endl;
	cout << "C) Retrieve Tour" << endl;
	cout << "D) Remove Tour" << endl;
	cout << "E) Remove All Tours" << endl;
	cout << "F) Load Tours" << endl;
	cout << "G) Peek current (+)" << endl;
	cout << "H) Peek current (-)" << endl;
	cout << "I) Increment current (+=)" << endl;
	cout << "J) Deincrement current (-=)" << endl;
	cout << "L) Set current to head (--)" << endl;
	cout << "M) Set current to end (++)" << endl;
	cout << "Q) Quit" << endl;
	cout << "<) Previous Menu" << endl;
	cout << "Selection: ";
}

void Menu :: printTourManipMenu() const
{
	cout << endl;
	cout << "Tour Manipulation Menu:" << endl;
	cout << "A) Add Attraction" << endl;
	cout << "B) Replace Attraction" << endl;
	cout << "C) Remove Attraction" << endl;
	cout << "D) Update Address" << endl;
	cout << "E) Update Time" << endl;
	cout << "F) Update Text" << endl;
	cout << "G) Test Equalities" << endl;
	cout << "Q) Quit" << endl;
	cout << "<) Go Back" << endl;
	cout << "Selection: ";
}

int Menu :: tourManipSelect(Node<Tour> * & tour, char & option)
{
	if(tour)
	{
		do
		{
			cout << tour << endl;
			printDineManipMenu(); 
			option = getChar();
			tourManipRead(tour, option);
		}
		while(option != 'q' && option != '<');
		if(option == '<')
			option = 'a';
		return 1;
	}
	else
		return -1;	
	
}

int Menu :: tourManipRead(Node<Tour> * & tour, char & option)
{
	switch(option)
	{
		case 'a':
			addAttraction(tour, option);		// += (string) operator
			break;
		case 'b':
			replaceAttraction(tour, option);	// -= (string) operator
			break;
		case 'c':
			removeAttraction(tour, option);		// -- (string) operator
			break;
		case 'd':
			updateAddress(tour, option);		// static cast (Activity) >> (istream) operator
			break;
		case 'e':
			updateTime(tour, option);		// static cast (Activity) += (int) operator
			break;
		case 'f':
			updateText(tour, option);		// << (ostream) operator
			break;
		case 'g':
			testEqualities(tour);			// equality operators (==, !=, <, <=, >, >=);
			break;
		case 'q':
			break;
		case '<':
			break;
		default:
			cout << "Invalid option..." << endl;
	}
	return 1;
}

void Menu :: addAttraction(Node<Tour> * & tour, char & option)
{
	if(tour)
	{
		cout << "Enter attraction: ";
		string buffer = getText();
		Tour temp; 
		temp << *tour;
		temp += buffer;
		*tour = temp;
	}
}

void Menu :: replaceAttraction(Node<Tour> * & tour, char & option)
{
	if(tour)
	{
		cout << "Enter attraction: ";
		string buffer = getText();
		Tour temp;
		temp << *tour;
		temp -= buffer;
		*tour = temp;
	}
}

void Menu :: removeAttraction(Node<Tour> * & tour, char & option)
{
	if(tour)
	{
		Tour temp;
		temp << *tour;
		--temp;
		*tour = temp;
	}
}

//////////// GAME MENU ////////////////

void Menu :: printGameMenu() const
{
	cout << endl;
	cout << "Game Menu:" << endl;
	cout << "A) Display Games" << endl;
	cout << "B) Insert Game" << endl;
	cout << "C) Retrieve Game" << endl;
	cout << "D) Remove Game" << endl;
	cout << "E) Remove All Games" << endl;
	cout << "F) Load Games" << endl;
	cout << "G) Peek current (+)" << endl;
	cout << "H) Peek current (-)" << endl;
	cout << "I) Increment current (+=)" << endl;
	cout << "J) Deincrement current (-=)" << endl;
	cout << "L) Set current to head (--)" << endl;
	cout << "M) Set current to end (++)" << endl;
	cout << "Q) Quit" << endl;
	cout << "<) Previous Menu" << endl;
	cout << "Selection: ";
}

void Menu :: printGameManipMenu() const
{
    cout << "Game Manipulation Menu:" << endl;
    cout << "A) Play Game" << endl;
    cout << "B) Increase Tiles" << endl;
    cout << "C) Update Risk" << endl;
    cout << "D) Update Chance" << endl;
    cout << "F) Update Chance Minus" << endl;
    cout << "G) Update Address" << endl;
    cout << "H) Update Time" << endl;
    cout << "I) Update Text" << endl;
    cout << "J) Reset Game" << endl;
    cout << "K) Test Equalities" << endl;
    cout << "Q) Quit" << endl;
    cout << "<) Go Back" << endl;
}

int Menu :: gameManipSelect(Node<Game> * & game, char & option)
{
	if(game)
	{
		do
		{
			cout << game << endl;
			printDineManipMenu(); 
			option = getChar();
			gameManipRead(game, option);
		}
		while(option != 'q' && option != '<');
		if(option == '<')
			option = 'a';
		return 1;
	}
	else
		return -1;	

}

int Menu :: gameManipRead(Node<Game> * & game, char & option)
{
	switch(option)
	{
		case 'a':
			playGame(game, option);			// ++ operator
			break;
		case 'b':
			increaseTiles(game, option);		// += (int) operator
			break;
		case 'c':
			updateRisk(game, option);		// -= (int) operator
			break;
		case 'd':
			updateChance(game, option);		// += (long) operator
			break;
		case 'f':
			updateChanceMinus(game, option);	// -= (long) operator
			break;
		case 'g':
			updateAddress(game, option);		// static cast (Activity) >> (istream) operator
			break;
		case 'h':
			updateTime(game, option);		// static cast (Activity) += (int) operator
			break;
		case 'i':
			updateText(game, option);		// << (string) operator
			break;
		case 'j':
			resetGame(game, option);		// -- operator
			break;
		case 'k':
			testEqualities(game);			// equality operators (==, !=, <, <=, >, >=)
			break;
		case 'q':
			break;
		case '<':
			break;
		default:
			cout << "Invalid option..." << endl;
	}
	return 1;
}

void Menu :: playGame(Node<Game> * & game, char & option)
{
	Game temp;
	temp << *game;
	++temp;
	*game = temp;
}

void Menu :: increaseTiles(Node<Game> * & game, char & option)
{
	cout << "Enter the amount of times to increase.\n";
	cout << "Total tiles cannot exceed " << MAX_TILE << ": ";
	int num = getInt();	
	Game temp;
	temp << *game;
	temp += num;
	*game = temp;
}

void Menu :: updateRisk(Node<Game> * & game, char & option)
{
	cout << "Change Risk factor (1 - " << MAX_TILE << "): ";
	int num = getInt();
	Game temp;
	temp << *game;
	temp -= num;
	*game = temp;
}

void Menu :: updateChance(Node<Game> * & game, char & option)
{
	cout << "Change Chance factor (6 - " << MAX_TILE << "): "; 
	int long num = getLong();
	Game temp;
	temp << *game;
	temp += num;
	*game = temp;
}

void Menu :: updateChanceMinus(Node<Game> * & game, char & option)
{
	cout << "Change Chance factor (-) (6 - " << MAX_TILE << "): "; 
	int long num = getLong();
	Game temp;
	temp << *game;
	temp -= num;
	*game = temp;
}

void Menu :: resetGame(Node<Game> * & game, char & option)
{
	cout << "Are you sure you want to reset?\nYou will lose your progress (y/n): ";
	if(yesNo(option))
	{
		Game temp;
		temp << *game;
		--temp;
		*game = temp;
	}
}

/////////// DINE MENU //////////////////

void Menu :: printDineMenu() const
{
	cout << endl;
	cout << "Dine Menu:" << endl;
	cout << "A) Display Dines" << endl;
	cout << "B) Insert Dine" << endl;
	cout << "C) Retrieve Dine" << endl;
	cout << "D) Remove Dine" << endl;
	cout << "E) Remove All Dines" << endl;
	cout << "F) Load Dines" << endl;
	cout << "G) Peek current (+)" << endl;
	cout << "H) Peek current (-)" << endl;
	cout << "I) Increment current (+=)" << endl;
	cout << "J) Deincrement current (-=)" << endl;
	cout << "L) Set current to head (--)" << endl;
	cout << "M) Set current to end (++)" << endl;
	cout << "Q) Quit" << endl;
	cout << "<) Previous Menu" << endl;
	cout << "Selection: ";
}

void Menu :: printDineManipMenu() const
{
	cout << "Dine Manipulation Menu:" << endl;
	cout << "A) Add Menu Item" << endl;
	cout << "B) Replace Menu Item" << endl;
	cout << "C) Remove Menu Item" << endl;
	cout << "D) Buy Menu Item" << endl;
	cout << "E) Unbuy Menu Item" << endl;
	cout << "F) Update Address" << endl;
	cout << "G) Update Time" << endl;
	cout << "H) Update Text" << endl;
	cout << "I) Test Equalities" << endl;
	cout << "Q) Quit" << endl;
	cout << "<) Go Back" << endl;
}

int Menu :: dineManipSelect(Node<Dine> * & dine, char & option)
{
	if(dine)
	{
		do
		{
			cout << dine << endl;
			printDineManipMenu(); 
			option = getChar();
			dineManipRead(dine, option);
		}
		while(option != 'q' && option != '<');
		if(option == '<')
			option = 'a';
		return 1;
	}
	else
		return -1;	
}

int Menu :: dineManipRead(Node<Dine> * & dine, char & option)
{
	switch(option)
	{
		case 'a':
			addMenuItem(dine, option);		// += operator
			break;
		case 'b':
			replaceMenuItem(dine, option);		// -= operator
			break;
		case 'c':
			removeMenuItem(dine, option);		// -- operator
			break;
		case 'd':
			buyMenuItem(dine, option);		// + operator
			break;
		case 'e':
			unbuyMenuItem(dine, option);		// - operator
			break;
		case 'f':
			updateAddress(dine, option);		// static cast (Activity) >> istream operator
			break;
		case 'g':
			updateTime(dine, option);		// static cast (Activity) += (int) operator
			break;
		case 'h':
			updateText(dine, option);		// << (string) operator
			break;
		case 'i':
			testEqualities(dine);			// equality operators (==, !=, <, <=, >, >=)
			break;
		case 'q':
			break;
		case '<':
			break;
		default:
			cout << "Invalid option..." << endl;
	}
	return 1;
}


void Menu :: addMenuItem(Node<Dine> * & dine, char & option)              // += operator
{
	cout << "Enter menu item name: ";
	string buffer = getText();
	cout << "Enter menu item price: ";
	double price = getDouble();
	Dine temp;
	temp << *dine;
	temp += buffer;
	temp += price;
	*dine = temp;
}

void Menu :: replaceMenuItem(Node<Dine> * & dine, char & option)          // -= operator
{
	cout << "Enter menu item name: ";
	string buffer = getText();
	cout << "Enter menu item price: ";
	double price = getDouble();
	Dine temp;
	temp << *dine;
	temp -= buffer;
	temp -= price;
	*dine = temp;
}

void Menu :: removeMenuItem(Node<Dine> * & dine, char & option)           // -- operator
{
	Dine temp;
	temp << *dine;
	--temp;
	*dine = temp;
}

double Menu :: buyMenuItem(Node<Dine> * & dine, char & option)              // + operator
{
	cout << "Menu item index to buy: ";
	int num = getInt();
	Dine temp;
	temp << *dine;
	return temp + num;
}

double Menu :: unbuyMenuItem(Node<Dine> * & dine, char & option)            // - operator
{
	cout << "Menu item index to unbuy: ";
	int num = getInt();
	Dine temp;
	temp << *dine;
	return temp - num;
}

void Menu :: createTour(Node<Tour> * & temp)
{
	cout << "Enter Location name: ";
	string location = getText();
	cout << "Enter address: ";
	string address = getText();
	cout << "Enter time (int): ";
	int time = getInt();
	cout << "Enter number of attactions (0 - 5): ";
	
	Tour tour {address, time, location};
	int attractions = getInt(0, 5);
	string name;
	for(int i = 1; i<=attractions;i++)
	{
		cout << "Enter name for attraction " << i <<  ": ";
		name = getText();
		tour += name;
	}
	temp = new Node<Tour>(tour);
	cout << "Tour created...";
}

void Menu :: createGame(Node<Game> * & temp)
{
	// Game(const std::string &, const int &, const std::string &, const int &);
	cout << "Enter save file name: ";
	string name = getText();
	cout << "Enter address: ";
	string address = getText();
	cout << "Enter time (int): ";
	int time = getInt();
	cout << "26 - " << MAX_TILE << "): ";
	int tile = getInt(26, MAX_TILE);
	
	Game game{address, time, name, tile};	
	temp = new Node<Game>(game);
	cout << "Game created...";
}

void Menu :: createDine(Node<Dine> * & temp)
{
	// Dine(const std::string &, const int &, const std::string &);
	cout << "Enter restaurant name: ";
	string name = getText();
	cout << "Enter address: ";
	string address = getText();
	cout << "Enter time (int): ";
	int time = getInt();
	cout << "Enter number of menu items, (0 - 5): ";
	
	Dine dine {address, time, name};
	int menuItems = getInt(0, 5);
	double price;

	for(int i = 1; i<=menuItems;i++)
	{
		cout << "Enter name for item " << i <<  ": ";
		name = getText();
		dine += name;

		cout << "Enter price for item " << i <<  ", ($1 - $25): ";
		price = getDouble(1.0, 25.0);
		dine += price;
	}
	temp = new Node<Dine>(dine);
	cout << "Dine created...";
}







