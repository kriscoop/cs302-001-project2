#include "dine.h"


        friend ostream & operator << (ostream & output, const Dine & op1);
        friend istream & operator >> (istream & input, Dine & op2);

        friend bool operator == (const Dine & op1, const Dine & op2);
        friend bool operator == (const Dine & op1, const string &);
        friend bool operator == (const string & op1, const Dine & op2);
        friend bool operator != (const Dine & op1, const Dine & op2);
        friend bool operator != (const Dine & op1, const string &);
        friend bool operator != (const string & op1, const Dine & op2);

	Dine & operator += (Dine & op1, const string & op2);
	Dine & operator += (Dine & op1, const double & op2);
	Dine & operator -= (Dine & op1, const string & op2);
	Dine & operator -= (Dine & op1, const double & op2);
	Dine operator = (const Dine & op1);
	Dine operator + (const string &)
	Dine operator - (const string &)

        friend bool operator < (const Dine & op1, const Dine & op2);
        friend bool operator < (const Dine & op1, const string &);
        friend bool operator < (const string & op1, const Dine & op2);
        friend bool operator <= (const Dine & op1, const Dine & op2);
        friend bool operator <= (const Dine & op1, const string &);
        friend bool operator <= (const string & op1, const Dine & op2);
        friend bool operator > (const Dine & op1, const Dine & op2);
        friend bool operator > (const Dine & op1, const string &);
        friend bool operator > (const string & op1, const Dine & op2);
        friend bool operator >= (const Dine & op1, const Dine & op2);
        friend bool operator >= (const Dine & op1, const string &);
        friend bool operator >= (const string & op1, const Dine & op2);
