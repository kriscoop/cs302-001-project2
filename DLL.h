/*
Name: Kristian Cooper
Class#: CS-302-001 
Project2
File: DLL.h
Description: 	This header file contains the templates for the my Node and DLL classes. 
		Refer to DLL.tpp for implementation and function expectations.
*/


#ifndef DLL_H
#define DLL_H

#include <iostream>

///////////// Node Implementation ////////////////////
template <class TYPE> class Node
{
public:
	Node();
	Node(std::istream &);
	Node(TYPE & someData);
	Node(const Node<TYPE> & aNode);
	~Node();

	std::istream & loadData(std::istream & input);

	Node<TYPE> * & getNext();
	Node<TYPE> * & getPrev();
	void setNext(Node<TYPE> * & node);
	void setPrev(Node<TYPE> * & node);

	/////////// Operator overload //////////////
	Node & operator = (const Node<TYPE> & op1);
	Node & operator = (const TYPE & op1);
	bool operator < (const Node<TYPE> & op1);
	template <class T>
	friend std::ostream & operator << (std::ostream & output, const Node<T> & node);
	template <class T>
	friend T & operator << (T & op1, const Node<T> & node);

private:
	Node<TYPE> * next;
	Node<TYPE> * prev;
	TYPE data;

	std::istream & load(std::istream & input);	
};


////////////// DLL Implementation /////////////////
template <class TYPE> class DLL
{
public:
	DLL();
	DLL(std::istream &);
	DLL(const Node<TYPE> & node);
	DLL(Node<TYPE> * & node);
	DLL(const DLL<TYPE> & aDLL);
	~DLL();

	void insert(Node<TYPE> * & aNode);
	int retrieve(const int & index, Node<TYPE> * & aNode);
	int remove(const int & index);
	int removeAll();
	int display();

	std::istream & loadData(std::istream & input);

	/////////////// operator overloads /////////////////
	template <class T>
	friend std::ostream & operator << (std::ostream & output, DLL<T> & op1);
	DLL<TYPE> & operator += (const int & op2);
	DLL<TYPE> & operator -= (const int & op2);
	DLL<TYPE> & operator ++ ();
	DLL<TYPE> & operator -- ();
	template <class T>
	friend Node<T> * operator + (DLL<T> & op1, const int & op2);
	template <class T>
	friend Node<T> * operator - (DLL<T> & op1, const int & op2);
	DLL & operator = (const DLL<TYPE> & op1);

private:
	Node<TYPE> * head;
	Node<TYPE> * current;
	
	//////////// Recursive Helper Functions ///////////////
	void destroy(Node<TYPE> * & current);
	void insert(Node<TYPE> * & current, Node<TYPE> * & aNode);
	int retrieve(Node<TYPE> * & current, const int & index, Node<TYPE> * & aNode);
	int remove(Node<TYPE> * & current, const int & index);
	void removeNode(Node<TYPE> * & current);
	std::ostream & print(Node<TYPE> * & current, std::ostream & output);
	void copyNode(Node<TYPE> * & source, Node<TYPE> * & copy);
	Node<TYPE> * & increment(const int & op1);
	Node<TYPE> * & increment(Node<TYPE> * & op1, const int & op2);
	void moveToEnd(Node<TYPE> * & current);
	std::istream & load(std::istream & input);	

};


#include "DLL.tpp"

#endif



