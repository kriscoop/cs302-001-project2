#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <cstdlib>
#include <ctime>

using namespace std;

std::string generateRandomStreet() {
    std::vector<std::string> streetNames = {
        "Maple",
        "Oak",
        "Elm",
        "Pine",
        "Cedar",
        "Birch",
        "Willow",
        "Spruce",
        "Poplar"
    };
    return streetNames[rand() % streetNames.size()] + " St";
};

std::string generateRandomCity() {
    std::vector<std::string> cityNames = {
        "Cityville",
        "Townsville",
        "Villageton",
        "Countryside",
        "Suburbia",
        "Veggieville"
    };
    return cityNames[rand() % cityNames.size()];
};

std::string generateRandomAddress() {
    std::string street = generateRandomStreet();
    std::string city = generateRandomCity();
    std::string number = std::to_string(rand() % 1000 + 1); // Generates a random number between 1 and 1000

    return number + " " + street + ", " + city;
};

    // Seed the random number generator with current time

std::string generateRandomFileName() {
    std::vector<std::string> fileNames = {
        "document1.txt",
        "report.docx",
        "image.jpg",
        "presentation.pptx",
        "spreadsheet.xlsx"
    };
    return fileNames[rand() % fileNames.size()]; // Selects a random filename from the list
};


int main()
{

    srand(time(0));
	int time = rand() % 24 + 1;
    int tiles = rand() % 75 + 26;
    int chance = rand() % tiles + 6;
    int danger = rand() % (chance - 3) + 4;
    int turn = rand() % 100 + 1;
    int lastRoll = rand() % chance + 1;
    int lastPosition = rand() % tiles;
    int states = rand() % 5 + 5;

    std::string address = generateRandomAddress();
	string fileName = generateRandomFileName();

    vector<string> stateDescrip = {"Starting Tile", "Blank Tile", "Trap Tile", "Boost Tile", "Star Tile", "Bomb Tile", "Moon Tile", "Sun Tile", "Special Tile", "Winning Tile"};
    vector<int> stateBonus = {0, 0, -1, 1, 2, -2, -3, 3, 4, 0}; 
    vector<int> tileState(tiles, 0);
    tileState[0] = 0;
    tileState[1] = 1;
    tileState[tiles - 1] = states - 1;
    for (int i = 2; i < tiles - 1; i++) {
        tileState[i] = rand() % (states - 2) + 1;
    }
    int bonus =  stateBonus[tileState[lastPosition]] * danger;
    int currPosition = lastPosition + lastRoll + bonus;
	if(currPosition >= tiles)
		currPosition = tiles -1;
	else if(currPosition < 0)
		currPosition = 0;

    cout<< time << "|" << address << "|" <<  fileName << "|"<< chance << "|" << danger << "|" << turn << "|" << lastRoll << "|" << bonus << "|" << lastPosition << "|" << currPosition << "|" << tiles << "|" << states << endl;
	
	int i = 0;
    for (i = 0; i < states-1; i++) {
        cout << stateBonus[i] << "|" << stateDescrip[i] << endl;
    }
        cout << stateBonus[9] << "|" << stateDescrip[9] << endl;
    for (i = 0; i < tiles; i++) {
        cout << tileState[i];
	
	if(i < tiles-1)
		cout << '|';
	else
		cout << endl;
    }

//    cout << "Data file generated successfully!" << endl;

    return 0;
}

