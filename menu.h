/*
Name: Kristian Cooper
Class#: CS-302-001 
Project2
File: menu.h
Description: 	This implements user menu. Includes helper templates to interact with all types of lists associated.
		Only client interactions are load and run. 
*/


#ifndef MENU_H
#define MENU_H

#include "DLL.h"
#include "CMD.h"

class Menu
{
public:
	Menu();
	Menu(istream & tourF, istream & gameF, istream & dineF);
	~Menu();

	int loadTours(istream & input);
	int loadGames(istream & input);
	int loadDines(istream & input);

	int run();

private:
	DLL<Tour> tours;
	DLL<Game> games;
	DLL<Dine> dines;

	//////////// top menu ///////////
	int printTopMenu() const;
	int topMenuSelect(char & option);
	int topMenuRead(char & option);


	//////////// Create Heirarchy objects ///////
	void createTour(Node<Tour> * & temp);
	void createGame(Node<Game> * & temp);
	void createDine(Node<Dine> * & temp);


	//////////// Tour Manipulation ////////////
	void printTourMenu() const;
	void printTourManipMenu() const;
	int tourManipSelect(Node<Tour> * & node, char & option);
	int tourManipRead(Node<Tour> * & node, char & option);

	//////////// Tour specialty functions ////////
	void addAttraction(Node<Tour> * & tour, char & option);			// += string
	void replaceAttraction(Node<Tour> * & tour, char & option);		// -= string
	void removeAttraction(Node<Tour> * & tour, char & option);		// -- operator


	//////////// Game Manipulation /////////////
	void printGameMenu() const;
	void printGameManipMenu() const;
	int gameManipSelect(Node<Game> * & node, char & option);
	int gameManipRead(Node<Game> * & node, char & option);

	///////////// Game specialty functions /////////
	void playGame(Node<Game> * & game, char & option);			// ++ operator
	void increaseTiles(Node<Game> * & game, char & option);			// += (int) operator
	void updateRisk(Node<Game> * & game, char & option);			// -= (int) operator
	void updateChance(Node<Game> * & game, char & option);			// += (long) operator
	void updateChanceMinus(Node<Game> * & game, char & option);		// += (long) operator
	void resetGame(Node<Game> * & game, char & option);			// -- operator


	///////////// Dine Manipulation /////////////////
	void printDineMenu() const;
	void printDineManipMenu() const;
	int dineManipSelect(Node<Dine> * & node, char & option);
	int dineManipRead(Node<Dine> * & node, char & option);

	//////////// Dine speciality functions ////////////
	void addMenuItem(Node<Dine> * & dine, char & option);			// += operator
	void replaceMenuItem(Node<Dine> * & dine, char & option);		// -= operator
	void removeMenuItem(Node<Dine> * & dine, char & option);		// -- operator
	double buyMenuItem(Node<Dine> * & dine, char & option);			// + operator
	double unbuyMenuItem(Node<Dine> * & dine, char & option);			// - operator


	//////////// Menu Helper template functinos (implemented in helper.tpp ////////////
	template<class TYPE> int listMenuSelect(DLL<TYPE> & list, char & option);
	template<class TYPE> void printListMenu(DLL<TYPE> & list) const;
	template<class TYPE> void plusCurrent(DLL<TYPE> & list);			// list + int
	template<class TYPE> void minusCurrent(DLL<TYPE> & list);			// list - int
	template<class TYPE> void plusEqualCurrent(DLL<TYPE> & list);			// list += int
	template<class TYPE> void minusEqualCurrent(DLL<TYPE> & list);			// list -= int
	template<class TYPE> int listMenuRead(DLL<TYPE> & list, char & option);
	template<class TYPE> void displayList(DLL<TYPE> & list) const;			// cout << operator
	template<class TYPE> void insertList(DLL<TYPE> & list);
	template<class TYPE> void retrieveList(DLL<TYPE> & list, char & option);
	template<class TYPE> void removeList(DLL<TYPE> & list, char & option);
	template<class TYPE> void removeAllList(DLL<TYPE> & list, char & option);
	template<class TYPE> void loadList(DLL<TYPE> & list, istream & input, char & option);

	// >> (static cast Activity)
	template<class TYPE> void updateAddress(Node<TYPE> * & node, char & option);
	// += (static cast Activity)
	template<class TYPE> void updateTime(Node<TYPE> * &  node, char & option);
	// >> operator
	template<class TYPE> void updateText(Node<TYPE> *  & node, char & option);

	/// ==, !=, <, <=, >, >= operators
	template<class TYPE> void testEqualities(Node<TYPE> * &  node, char & option);

	/////////// template helpers to test comparison operators ////////////
	template<class TYPE> void testEqualities(Node<TYPE> * &);
	template<class TYPE> void isEqual(TYPE op1, TYPE op2);
	template<class TYPEA, class TYPEB> void isEqual(TYPEA op1, TYPEB op2);
	template<class TYPE> void isNotEqual(TYPE op1, TYPE op2);
	template<class TYPEA, class TYPEB> void isNotEqual(TYPEA op1, TYPEB op2);
	template<class TYPE> void lessThan(TYPE op1, TYPE op2);
	template<class TYPEA, class TYPEB> void lessThan(TYPEA op1, TYPEB op2);
	template<class TYPE> void lessThanEqual(TYPE op1, TYPE op2);
	template<class TYPEA, class TYPEB> void lessThanEqual(TYPEA op1, TYPEB op2);
	template<class TYPE> void greatThan(TYPE op1, TYPE op2);
	template<class TYPEA, class TYPEB> void greatThan(TYPEA op1, TYPEB op2);
	template<class TYPE> void greatThanEqual(TYPE op1, TYPE op2);
	template<class TYPEA, class TYPEB> void greatThanEqual(TYPEA op1, TYPEB op2);

};

#include "helpers.tpp"

#endif

