#ifndef TOUR_H
#define TOUR_H


class Tour:
{
public:
        friend ostream & operator << (ostream & output, const Tour & op1);
        friend istream & operator >> (istream & input, Tour & op2);

        friend bool operator == (const Tour & op1, const Tour & op2);
        friend bool operator == (const Tour & op1, const string &);
        friend bool operator == (const string & op1, const Tour & op2);
        friend bool operator != (const Tour & op1, const Tour & op2);
        friend bool operator != (const Tour & op1, const string &);
        friend bool operator != (const string & op1, const Tour & op2);

	Tour & operator += (Tour & op1, const string & op2);
	Tour operator = (const Tour & op1);

	Tour operator < (
	Tour operator <= (
	Tour operator > (
	Tour operator >= (
	
	Tour operator += (
	Tour operator -= (
	Tour operator + (
	Tour operator - (

protected:
	char * location;
	

};

#endif
