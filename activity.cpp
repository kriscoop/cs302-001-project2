#include "activity.h"

///////////////////// Activity Implementation /////////////

Activity :: Activity(): time(0), address(nullptr)
{
}

Activity :: Activity(const Activity & to_copy): time(0), address(nullptr)
{
	time = to_copy.time;
	copy(address, to_copy.address);
}

Activity :: Activity(std::istream & input): time(0), address(nullptr)
{
	load(input);
}

Activity :: Activity(const std::string & aStr, const int & aTime): time(aTime), address(nullptr)
{
	const char * buffer = aStr.c_str();
	copy(address, buffer);
}

Activity :: ~Activity()
{
	delete [] address;
}

std::istream & Activity :: loadData(std::istream & input)
{
	return load(input);
}

std::ostream & operator << (std::ostream & output, const Activity & op1)
{
	output << op1.address << " | "  << op1.time << " | ";
	return output;	
}

std::istream & operator >> (std::istream & input, Activity & op2)
{
	char buffer[MAX_STRING];
	const char * ptr = buffer;
	input.getline(buffer, MAX_STRING, '\n');

	op2.copy(op2.address,ptr);
	return input;
}

bool operator == (const Activity & op1, const Activity & op2)
{
	int condition = op1.compare(op1.address, op2.address);
	if(condition == 0)
		return true;
	else
			return false;
}

bool operator == (const Activity & op1, const std::string & op2)
{
	const char * buffer = op2.c_str();
	int condition = op1.compare(op1.address, buffer);
	if(condition == 0)
		return true;
	else
			return false;
}

bool operator == (const std::string & op1, const Activity & op2)
{
	const char * buffer = op1.c_str();
	int condition = op2.compare(buffer, op2.address);
	if(condition == 0)
		return true;
	else
			return false;
}
	
bool operator != (const Activity & op1, const Activity & op2)
{
	int condition = op1.compare(op1.address, op2.address);
	if(condition != 0)
		return true;
	else
			return false;
}

bool operator != (const Activity & op1, const std::string & op2)
{
	const char * buffer = op2.c_str();
	int condition = op1.compare(op1.address, buffer);
	if(condition != 0)
		return true;
	else
			return false;
}

bool operator != (const std::string & op1, const Activity & op2)
{
	const char * buffer = op1.c_str();
	int condition = op2.compare(buffer, op2.address);
	if(condition != 0)
		return true;
	else
			return false;
}

bool operator < (const Activity & op1, const Activity & op2)
{
	int condition = op1.compare(op1.address, op2.address);
	if(condition < 0)
		return true;
	else
			return false;
}

bool operator < (const Activity & op1, const std::string & op2)
{
	const char * buffer = op2.c_str();
	int condition = op1.compare(op1.address, buffer);
	if(condition < 0)
		return true;
	else
			return false;
}

bool operator < (const std::string & op1, const Activity & op2)
{
	const char * buffer = op1.c_str();
	int condition = op2.compare(buffer, op2.address);
	if(condition < 0)
		return true;
	else
			return false;
}

bool operator <= (const Activity & op1, const Activity & op2)
{
	int condition = op1.compare(op1.address, op2.address);
	if(condition <= 0)
		return true;
	else
			return false;
}

bool operator <= (const Activity & op1, const std::string & op2)
{
	const char * buffer = op2.c_str();
	int condition = op1.compare(op1.address, buffer);
	if(condition <= 0)
		return true;
	else
			return false;
}

bool operator <= (const std::string & op1, const Activity & op2)
{
	const char * buffer = op1.c_str();
	int condition = op2.compare(buffer, op2.address);
	if(condition <= 0)
		return true;
	else
			return false;
}

bool operator > (const Activity & op1, const Activity & op2)
{
	int condition = op1.compare(op1.address, op2.address);
	if(condition > 0)
		return true;
	else
			return false;
}

bool operator > (const Activity & op1, const std::string & op2)
{
	const char * buffer = op2.c_str();
	int condition = op1.compare(op1.address, buffer);
	if(condition > 0)
		return true;
	else
			return false;
}

bool operator > (const std::string & op1, const Activity & op2)
{
	const char * buffer = op1.c_str();
	int condition = op2.compare(buffer, op2.address);
	if(condition > 0)
		return true;
	else
			return false;
}

bool operator >= (const Activity & op1, const Activity & op2)
{
	int condition = op1.compare(op1.address, op2.address);
	if(condition >= 0)
		return true;
	else
			return false;
}

bool operator >= (const Activity & op1, const std::string & op2)
{
	const char * buffer = op2.c_str();
	int condition = op1.compare(op1.address, buffer);
	if(condition >= 0)
		return true;
	else
			return false;
}

bool operator >= (const std::string & op1, const Activity & op2)
{
	const char * buffer = op1.c_str();
	int condition = op2.compare(buffer, op2.address);
	if(condition >= 0)
		return true;
	else
			return false;
}

Activity & Activity :: operator += (const std::string & op1)
{
	const char * buffer = op1.c_str();
	catPlus(address, buffer);
		
	return *this;
}

Activity & Activity :: operator += (const int & op1)
{
	incrementTime(op1);
	return *this;
}

Activity & Activity :: operator -= (const std::string & op1)
{
	const char * buffer = op1.c_str();
	catMinus(address, buffer);
		
	return *this;
}

Activity & Activity :: operator -= (const int & op1)
{
	int increment = op1 * -1;
	incrementTime(increment);
	return *this;
}

Activity & Activity :: operator = (const Activity & op1)
{
	if(this == &op1)
		return *this;
	if(address)
	{
		delete [] address;
		address = nullptr;
	}
	copy(address, op1.address);
	time = op1.time;
	return *this;
}

Activity operator + (const Activity & op1, const std::string & op2)
{
	Activity newActivity(op1);
	newActivity += op2;
	return newActivity;
}

Activity operator + (const Activity & op1, const int & op2)
{
	Activity newActivity(op1);
	newActivity += op2;
	return newActivity;
}

Activity operator - (const Activity & op1, const std::string & op2)
{
	Activity newActivity(op1);
	newActivity -= op2;
	return newActivity;
}

Activity operator - (const Activity & op1, const int & op2)
{	
	Activity newActivity(op1);
	newActivity -= op2;
	return newActivity;

}

void Activity :: copy(char * & copy_to, const char * to_copy)
{
	try
	{
		if(!to_copy)
			throw to_copy;
		if(copy_to)
			delete [] copy_to;
		if(to_copy)
		{
			copy_to  = new char[strlen(to_copy) + 1];
			strcpy(copy_to, to_copy);
		}
	}
	catch(char * ex)
	{
		std::cout << "Cannot copy empty cstring" << std::endl;
	}
}

int Activity :: compare(const char * op1, const char * op2) const
{
	try
	{
		if(!op1) 
			throw op1;
		else if(!op2)
			throw op2;
	
		return strcmp(op1, op2);
	}
	catch(char * ex)
	{
		std::cout << "cstring is empty." << std::endl;
	}
	return 0;
}

void Activity :: catPlus(char * & op1, const char * & op2)
{
	int size = 0;

	
	if(op1)
	{
		size = strlen(op1) + strlen(op2) + 1;
		char * newstring = new char[size];
		strcpy(newstring, op1);
		strcat(newstring, op2);
		delete [] op1;
		op1 = newstring;
	}
	else
	{
		size = strlen(op2) + 1;
		char * op1 = new char[size];
		strcpy(op1, op2);
	}

}

void Activity :: catMinus(char * & op1, const char * & op2)
{
	int size1 = 0;
	int size2 = strlen(op2);

	if(op1)
	{
		size1 = strlen(op1);
		if(size1 > size2)
		{
			char * newAddress = new char[size1 + 1];
			int overwrite = size1 - size2;
			strncpy(newAddress, op2, overwrite);
			newAddress[overwrite] = '\0';
			strcat(newAddress, op2);
			delete [] op1;
			op1 = newAddress;
		}
		else
		{
			char * newAddress = new char[size2 + 1];
			strcpy(newAddress, op2);
			delete [] op1;
			op1 = newAddress;
		}
	}
	else
	{
		size2 = sizeof(op2);
		char * op1 = new char[size2];
		strcpy(op1, op2);
	}
}
 
void Activity :: incrementTime(const int & op1)
{
	time += op1;
	time %= 24;
	if(time < 0)
		time *= -1;
}

std::istream & Activity :: load(std::istream & input)
{
	if(input)
	{
		char buffer [MAX_STRING];
		input >> time;
		input.ignore(1, '|');

		if(!input.eof())
		{
			input.getline(buffer, MAX_STRING,'|');	
			copy(address, buffer);
		}
	}
	return input;
}

/////////////// Tour Implementation ////////////////

Tour :: Tour(): location(nullptr)
{
}

Tour :: Tour(std::istream & input): Activity(input), location(nullptr)
{
	load(input);
}

Tour :: Tour(const std::string & address, const int & time, const std::string & aString): Activity(address, time), location(nullptr)
{
	const char * buffer = aString.c_str();
	copy(location, buffer);
}

Tour :: Tour(const Tour & to_copy): Activity(to_copy), location(nullptr)
{
	Activity::copy(location, to_copy.location);
	sites = to_copy.sites;
}

Tour :: ~Tour()
{
	delete [] location;
}

std::istream & Tour :: loadData(std::istream & input)
{
	Activity::load(input);
	return load(input);
}

std::ostream & operator << (std::ostream & output, const Tour & op1)
{
	int i = 0;
	output << op1.location << '|' << op1.address << '|' << op1.time;
	for(auto const & it : op1.sites)
	{
		i++;
		output << std::endl << i << ".\t" << it; 
	}

	return output;
}

std::istream & operator >> (std::istream & input, Tour & op2)
{
	if(op2.location)
	{
		delete [] op2.location;
		op2.location = nullptr;
	}
	
	std::string buffer;
	getline(input, buffer);
	const char * temp = buffer.c_str();
	op2.Activity::copy(op2.location, temp);

	return input;
}

bool operator == (const Tour & op1, const Tour & op2)
{
	int conditionB = op1.compare(op1.location, op2.location);

	if(conditionB == 0)
		return true;
	else
		return false;
}

bool operator == (const Tour & op1, const std::string & op2)
{
	const char * buffer = op2.c_str();
	int condition = op1.compare(op1.location, buffer);
	
	if(condition == 0)
		return true;
	else
		return false;
}

bool operator == (const std::string & op1, const Tour & op2)
{
	const char * buffer = op1.c_str();
	int condition = op2.compare(op2.location, buffer);
	
	if(condition == 0)
		return true;
	else
		return false;
}

bool operator != (const Tour & op1, const Tour & op2)
{
	int conditionB = op1.compare(op1.location, op2.location);

	if(conditionB != 0)
		return true;
	else
		return false;
}

bool operator != (const Tour & op1, const std::string & op2)
{
	const char * buffer = op2.c_str();
	int condition = op1.compare(op1.location, buffer);
	
	if(condition != 0)
		return true;
	else
		return false;
}

bool operator != (const std::string & op1, const Tour & op2)
{
	const char * buffer = op1.c_str();
	int condition = op2.compare(op2.location, buffer);
	
	if(condition != 0)
		return true;
	else
		return false;
}

bool operator < (const Tour & op1, const Tour & op2)
{
	int conditionB = op1.compare(op1.location, op2.location);

	if(conditionB < 0)
		return true;
	else
		return false;
}

bool operator < (const Tour & op1, const std::string & op2)
{
	const char * buffer = op2.c_str();
	int condition = op1.compare(op1.location, buffer);
	
	if(condition < 0)
		return true;
	else
		return false;
}

bool operator < (const std::string & op1, const Tour & op2)
{
	const char * buffer = op1.c_str();
	int condition = op2.compare(op2.location, buffer);
	
	if(condition < 0)
		return true;
	else
		return false;
}

bool operator <= (const Tour & op1, const Tour & op2)
{
	int conditionB = op1.compare(op1.location, op2.location);

	if(conditionB <= 0)
		return true;
	else
		return false;
}

bool operator <= (const Tour & op1, const std::string & op2)
{
	const char * buffer = op2.c_str();
	int condition = op1.compare(op1.location, buffer);
	
	if(condition <= 0)
		return true;
	else
		return false;
}

bool operator <= (const std::string & op1, const Tour & op2)
{
	const char * buffer = op1.c_str();
	int condition = op2.compare(op2.location, buffer);
	
	if(condition <= 0)
		return true;
	else
		return false;
}

bool operator > (const Tour & op1, const Tour & op2)
{
	int conditionB = op1.compare(op1.location, op2.location);

	if(conditionB > 0)
		return true;
	else
		return false;
}

bool operator > (const Tour & op1, const std::string & op2)
{
	const char * buffer = op2.c_str();
	int condition = op1.compare(op1.location, buffer);
	
	if(condition > 0)
		return true;
	else
		return false;
}

bool operator > (const std::string & op1, const Tour & op2)
{
	const char * buffer = op1.c_str();
	int condition = op2.compare(op2.location, buffer);
	
	if(condition > 0)
		return true;
	else
		return false;
}

bool operator >= (const Tour & op1, const Tour & op2)
{
	int conditionB = op1.compare(op1.location, op2.location);

	if(conditionB >= 0)
		return true;
	else
		return false;
}

bool operator >= (const Tour & op1, const std::string & op2)
{
	const char * buffer = op2.c_str();
	int condition = op1.compare(op1.location, buffer);
	
	if(condition >= 0)
		return true;
	else
		return false;
}

bool operator >= (const std::string & op1, const Tour & op2)
{
	const char * buffer = op1.c_str();
	int condition = op2.compare(op2.location, buffer);
	
	if(condition >= 0)
		return true;
	else
		return false;
}

Tour & Tour :: operator += (const std::string & op2)
{
	sites.push_back(op2);
	return *this;
}

Tour & Tour :: operator -= (const std::string & op2)
{
	sites.pop_back();
	sites.push_back(op2);
	return *this;
}

Tour & Tour :: operator = (const Tour & op1)
{
	copy(address, op1.address);
	copy(location, op1.location);
	time = op1.time;
	sites = op1.sites;
	return *this;
}

Tour & Tour :: operator -- ()
{
	if(!sites.empty())
		sites.pop_back();
	return *this;
}

std::istream & Tour :: load(std::istream & input)
{
	if(input)
	{
		char buffer [MAX_STRING];
		input.getline(buffer, MAX_STRING, '|');

		if(!input.eof())
		{
			copy(location, buffer);
			int attractions;
			input >> attractions;
			input.ignore(1, '\n');

			for(int i = 0; i < attractions && !input.eof(); i++)
			{
				input.getline(buffer, MAX_STRING, '\n');
				sites.push_back(buffer);
			}
		}		
	}

	return input;	
}


//////////// Game Implementation /////////////////

Game :: Game(): saveName("Error"), chance(6), danger(3), tiles(0), currPosition(0), lastPosition(0), turn(0), lastRoll(0), bonus(0), states(0)
{
	initializeTiles();
}

Game :: Game(std::istream & input): Activity(input), saveName("Error"), chance(6), danger(3), tiles(0), currPosition(0), lastPosition(0), turn(0), lastRoll(0), bonus(0), states(0)
{
	load(input);
}

Game :: Game(const std::string & address, const int & time, const std::string & aString, const int & aNum): Activity(address, time), saveName(aString), chance(6), danger(3), tiles(0), currPosition(0), lastPosition(0), turn(0), lastRoll(0), bonus(0), states(0)
{
	if(aNum > 0 && aNum <= MAX_TILE)
		tiles = aNum;
	else
		tiles = MAX_TILE;
	initializeTiles();

}

Game :: ~Game()
{
}

std::istream & Game :: loadData(std::istream & input)
{
	Activity::load(input);
	return load(input);
}

std::ostream & operator << (std::ostream & output, const Game & op1)
{
	output << "File: " << op1.saveName;
	output << " | " << static_cast<const Activity & >(op1) << std::endl;
	output << "Progress:\nTurn: " << op1.turn << "\tChance: 1/" << op1.chance << "\tRisk: " << op1.danger << std::endl;

	if(op1.turn != 0)
	{
		output << "Currently on " + op1.stateDescrip.at(op1.tileStates.at(op1.currPosition));
		output << ", " << op1.currPosition + 1 << '/' << op1.tiles << std::endl;
		output << "Previously on " + op1.stateDescrip.at(op1.tileStates.at(op1.lastPosition));
		output << " " << op1.lastPosition + 1 << '/' << op1.tiles << std::endl;
		output << "Rolled " << op1.lastRoll << " on the die to land on " ;
		output << op1.stateDescrip.at(op1.tileStates.at(op1.lastPosition));
		output << " (+ " << op1.bonus << " Bonus)";
	}
	else
	{
		output << "Currently on " + op1.stateDescrip.at(op1.currPosition);
		output << ", " << op1.currPosition + 1 << '/' << op1.tiles;
	}

	return output;
}

std::istream & operator >> (std::istream & input, Game & op2)
{
	getline(input, op2.saveName);
	return input;
}

bool operator == (const Game & op1, const Game & op2)
{
	const char * bufferA = op1.saveName.c_str();
	const char * bufferB = op2.saveName.c_str();
	int conditionB = op1.compare(bufferA, bufferB);

	if(conditionB == 0)
		return true;
	else
		return false;
}

bool operator == (const Game & op1, const std::string & op2)
{
	const char * buffer = op2.c_str();
	const char * bufferA = op1.saveName.c_str();
	int condition = op1.compare(bufferA, buffer);
	
	if(condition == 0)
		return true;
	else
		return false;
}

bool operator == (const std::string & op1, const Game & op2)
{
	const char * buffer = op1.c_str();
	const char * bufferB = op2.saveName.c_str();
	int condition = op2.compare(bufferB, buffer);
	
	if(condition == 0)
		return true;
	else
		return false;
}

bool operator != (const Game & op1, const Game & op2)
{
	const char * bufferA = op1.saveName.c_str();
	const char * bufferB = op2.saveName.c_str();
	int conditionB = op1.compare(bufferA, bufferB);

	if(conditionB != 0)
		return true;
	else
		return false;
}

bool operator != (const Game & op1, const std::string & op2)
{
	const char * buffer = op2.c_str();
	const char * bufferA = op1.saveName.c_str();
	int condition = op1.compare(bufferA, buffer);
	
	if(condition != 0)
		return true;
	else
		return false;
}

bool operator != (const std::string & op1, const Game & op2)
{
	const char * buffer = op1.c_str();
	const char * bufferB = op2.saveName.c_str();
	int condition = op2.compare(bufferB, buffer);
	
	if(condition != 0)
		return true;
	else
		return false;
}

bool operator < (const Game & op1, const Game & op2)
{
	const char * bufferA = op1.saveName.c_str();
	const char * bufferB = op2.saveName.c_str();
	int conditionB = op1.compare(bufferA, bufferB);

	if(conditionB < 0)
		return true;
	else
		return false;
}

bool operator < (const Game & op1, const std::string & op2)
{
	const char * buffer = op2.c_str();
	const char * bufferA = op1.saveName.c_str();
	int condition = op1.compare(bufferA, buffer);
	
	if(condition < 0)
		return true;
	else
		return false;
}

bool operator < (const std::string & op1, const Game & op2)
{
	const char * buffer = op1.c_str();
	const char * bufferB = op2.saveName.c_str();
	int condition = op2.compare(bufferB, buffer);
	
	if(condition < 0)
		return true;
	else
		return false;
}

bool operator <= (const Game & op1, const Game & op2)
{
	const char * bufferA = op1.saveName.c_str();
	const char * bufferB = op2.saveName.c_str();
	int conditionB = op1.compare(bufferA, bufferB);

	if(conditionB <= 0)
		return true;
	else
		return false;
}

bool operator <= (const Game & op1, const std::string & op2)
{
	const char * buffer = op2.c_str();
	const char * bufferA = op1.saveName.c_str();
	int condition = op1.compare(bufferA, buffer);
	
	if(condition <= 0)
		return true;
	else
		return false;
}

bool operator <= (const std::string & op1, const Game & op2)
{
	const char * buffer = op1.c_str();
	const char * bufferB = op2.saveName.c_str();
	int condition = op2.compare(bufferB, buffer);
	
	if(condition <= 0)
		return true;
	else
		return false;
}

bool operator > (const Game & op1, const Game & op2)
{
	const char * bufferA = op1.saveName.c_str();
	const char * bufferB = op2.saveName.c_str();
	int conditionB = op1.compare(bufferA, bufferB);

	if(conditionB > 0)
		return true;
	else
		return false;
}

bool operator > (const Game & op1, const std::string & op2)
{
	const char * buffer = op2.c_str();
	const char * bufferA = op1.saveName.c_str();
	int condition = op1.compare(bufferA, buffer);
	
	if(condition > 0)
		return true;
	else
		return false;
}

bool operator > (const std::string & op1, const Game & op2)
{
	const char * buffer = op1.c_str();
	const char * bufferB = op2.saveName.c_str();
	int condition = op2.compare(bufferB, buffer);
	
	if(condition > 0)
		return true;
	else
		return false;
}

bool operator >= (const Game & op1, const Game & op2)
{
	const char * bufferA = op1.saveName.c_str();
	const char * bufferB = op2.saveName.c_str();
	int conditionB = op1.compare(bufferA, bufferB);

	if(conditionB >= 0)
		return true;
	else
		return false;
}

bool operator >= (const Game & op1, const std::string & op2)
{
	const char * buffer = op2.c_str();
	const char * bufferB = op1.saveName.c_str();
	int condition = op1.compare(bufferB, buffer);
	
	if(condition >= 0)
		return true;
	else
		return false;
}

bool operator >= (const std::string & op1, const Game & op2)
{
	const char * bufferA = op1.c_str();
	const char * bufferB = op2.saveName.c_str();
	int condition = op2.compare(bufferB, bufferA);
	
	if(condition >= 0)
		return true;
	else
		return false;
}

Game & Game :: operator += (const int & op1)
{
	if(op1 > 0 && tiles < MAX_TILE)
	{
		int num = op1 + tiles;
		if(num > MAX_TILE)
			num -= MAX_TILE;
		increaseTiles(num);
	}

	return *this;
}

Game & Game :: operator -= (const int & op1)
{
	int num = danger + op1;
	if(num > 0)
		danger = num;
	else
		danger = 1;
	return *this;
}

Game & Game :: operator += (const long & op1)
{
	long num = chance + op1;

	if(num >= 4)
		chance = num;
	else if(num < 4)
		chance = 4;
	else if(num > MAX_TILE)
		chance = MAX_TILE;
	
	return *this;
}

Game & Game :: operator -= (const long & op1)
{
	long num = chance - op1;

	if(num >= 4)
		chance = num;
	else if(num < 4)
		chance = 4;
	else if(num > MAX_TILE)
		chance = MAX_TILE;

	
	
	return *this;
}

Game & Game :: operator ++ ()
{
	time_t current_time = std::time(NULL);
	std::srand((unsigned) current_time);
	lastRoll = (std::rand() % chance) + 1;

	progress(lastRoll);	

	return *this;
}

Game & Game :: operator -- ()
{
	turn = 0;
	currPosition = 0;
	lastPosition = 0;
	return *this;
}

void Game :: initializeTiles()
{
	std::vector<std::string> defaultStateDescriptions {"Starting Tile", "Blank Tile", "Trap Tile", "Boost Tile", "Win Tile"};
	stateBonus.fill(0);
	stateBonus.at(2) = -1;
	stateBonus.at(3) = 1;

	const int DEFAULT_STATE = 5;
	states = DEFAULT_STATE;

	int DEFAULT_TILE = 0;
	if(tiles <= MAX_TILE && tiles > 0)
		DEFAULT_TILE = tiles;
	else
		DEFAULT_TILE = MAX_TILE;

	for(int i = 0; i < DEFAULT_STATE;i++)
		stateDescrip.at(i) = defaultStateDescriptions.at(i);
		
	std::srand((unsigned) std::time(NULL));
	int random;
	
	tileStates.at(0) = 0;

	for(int i = 1; i<DEFAULT_TILE; i++)
	{
		random = (std::rand() % (DEFAULT_STATE - 2) + 1);

		if(i == DEFAULT_TILE - 1)
			tileStates.at(i) = DEFAULT_STATE - 1;
		else
			tileStates.at(i) = random;
	}
}

void Game :: increaseTiles(const int & increase)
{
	std::srand((unsigned) std::time(NULL));
	int random;
	int newTiles = tiles + increase;
	
	for(int i = 0; i< increase; i++)
	{
		random = (std::rand() % (5 - 2) + 1);
		if(i == newTiles - 1)
			tileStates.at(tiles - 1 + i) = 5 - 1;
		else
			tileStates.at(tiles -1 + i) = random;
	}
	tiles = newTiles;
}

void Game :: progress(const int & roll)
{
	if(currPosition < tiles)
	{
		turn++;
		lastPosition = currPosition;
		currPosition += roll;
		
		checkState();
	}
}

void Game :: checkState()
{
	if(currPosition > tiles)
		currPosition = tiles - 1;
	else if(currPosition < 0)
		currPosition = 0;

	bonus = danger * stateBonus.at(tileStates.at(currPosition));
	if(bonus != 0)
	{
		lastPosition = currPosition;
		currPosition += bonus;

		if(currPosition > tiles)
			currPosition = tiles - 1;
		else if(currPosition < 0)
			currPosition = 0;
	}
}

std::istream & Game :: load(std::istream & input)
{
	if(input)
	{
		char buffer [MAX_STRING];
		input.getline(buffer, MAX_STRING, '|');
		stateBonus.fill(0);
		
		if(!input.eof())
		{
			saveName = buffer;
			input >> chance;
			input.ignore(1, '|');
				
			input >> danger;
			input.ignore(1, '|');
		
			input >> turn;
			input.ignore(1, '|');
			
			input >> lastRoll;
			input.ignore(1, '|');

			input >> bonus;
			input.ignore(1, '|');

			input >> lastPosition;
			input.ignore(1, '|');
			
			input >> currPosition;
			input.ignore(1, '|');
			
			input >> tiles;
			input.ignore(1, '|');

			input >> states;
			input.ignore(1, '\n');

			for(int i = 0; i<states;i++)
			{
				input >> stateBonus.at(i);
				input.ignore(1, '|');

				input.getline(buffer, MAX_STRING, '\n');

				stateDescrip.at(i) = buffer;
			}
			
			input >> tileStates.at(0);
			for(int i = 1; i<tiles; i++)
			{
				input.ignore(1, '|'); 
				input >> tileStates.at(i);
			}
			input.ignore(1, '\n'); 
		}
	}
	return input;
}
		
/////////// Dine Implementation //////////////


Dine :: Dine(): name("Error")
{
}

Dine :: Dine(std::istream & input): Activity(input)
{
	load(input);
}

Dine :: Dine(const std::string & address, const int & time, const std::string & aString): Activity(address, time), name(aString)
{
}

Dine :: ~Dine()
{
}

std::istream & Dine :: loadData(std::istream & input)
{
	Activity::load(input);
	return load(input);
}

std::ostream & operator << (std::ostream & output, const Dine & op1)
{
	int i = 0;
	output << op1.name << " | " << static_cast<const Activity &>(op1);
	
	if(!op1.menu.empty() && !op1.prices.empty())
	{
		int sizeM = op1.menu.size();

		for(int j = 0; j < sizeM;j++)
		{
			i++;
			output << std::endl << i << ".\t" << op1.prices.at(j) << '\t' << op1.menu.at(j); 
		}
	}

	return output;
}

std::istream & operator >> (std::istream & input, Dine & op2)
{
	getline(input, op2.name);
	return input;
}

bool operator == (const Dine & op1, const Dine & op2)
{
	const char * bufferA = op1.name.c_str();
	const char * bufferB = op2.name.c_str();
	int conditionB = op1.compare(bufferA, bufferB);

	if(conditionB == 0)
		return true;
	else
		return false;
}

bool operator == (const Dine & op1, const std::string & op2)
{
	const char * buffer = op2.c_str();
	const char * bufferA = op1.name.c_str();
	int condition = op1.compare(bufferA, buffer);
	
	if(condition == 0)
		return true;
	else
		return false;
}

bool operator == (const std::string & op1, const Dine & op2)
{
	const char * buffer = op1.c_str();
	const char * bufferB = op2.name.c_str();
	int condition = op2.compare(bufferB, buffer);
	
	if(condition == 0)
		return true;
	else
		return false;
}

bool operator != (const Dine & op1, const Dine & op2)
{
	const char * bufferA = op1.name.c_str();
	const char * bufferB = op2.name.c_str();
	int conditionB = op1.compare(bufferA, bufferB);

	if(conditionB != 0)
		return true;
	else
		return false;
}

bool operator != (const Dine & op1, const std::string & op2)
{
	const char * buffer = op2.c_str();
	const char * bufferA = op1.name.c_str();
	int condition = op1.compare(bufferA, buffer);
	
	if(condition != 0)
		return true;
	else
		return false;
}

bool operator != (const std::string & op1, const Dine & op2)
{
	const char * buffer = op1.c_str();
	const char * bufferB = op2.name.c_str();
	int condition = op2.compare(bufferB, buffer);
	
	if(condition != 0)
		return true;
	else
		return false;
}

bool operator < (const Dine & op1, const Dine & op2)
{
	const char * bufferA = op1.name.c_str();
	const char * bufferB = op2.name.c_str();
	int conditionB = op1.compare(bufferA, bufferB);

	if(conditionB < 0)
		return true;
	else
		return false;
}

bool operator < (const Dine & op1, const std::string & op2)
{
	const char * buffer = op2.c_str();
	const char * bufferA = op1.name.c_str();
	int condition = op1.compare(bufferA, buffer);
	
	if(condition < 0)
		return true;
	else
		return false;
}

bool operator < (const std::string & op1, const Dine & op2)
{
	const char * buffer = op1.c_str();
	const char * bufferB = op2.name.c_str();
	int condition = op2.compare(bufferB, buffer);
	
	if(condition < 0)
		return true;
	else
		return false;
}

bool operator <= (const Dine & op1, const Dine & op2)
{
	const char * bufferA = op1.name.c_str();
	const char * bufferB = op2.name.c_str();
	int conditionB = op1.compare(bufferA, bufferB);

	if(conditionB <= 0)
		return true;
	else
		return false;
}

bool operator <= (const Dine & op1, const std::string & op2)
{
	const char * buffer = op2.c_str();
	const char * bufferA = op1.name.c_str();
	int condition = op1.compare(bufferA, buffer);
	
	if(condition <= 0)
		return true;
	else
		return false;
}

bool operator <= (const std::string & op1, const Dine & op2)
{
	const char * buffer = op1.c_str();
	const char * bufferB = op2.name.c_str();
	int condition = op2.compare(bufferB, buffer);
	
	if(condition <= 0)
		return true;
	else
		return false;
}

bool operator > (const Dine & op1, const Dine & op2)
{
	const char * bufferA = op1.name.c_str();
	const char * bufferB = op2.name.c_str();
	int conditionB = op1.compare(bufferA, bufferB);

	if(conditionB > 0)
		return true;
	else
		return false;
}

bool operator > (const Dine & op1, const std::string & op2)
{
	const char * buffer = op2.c_str();
	const char * bufferA = op1.name.c_str();
	int condition = op1.compare(bufferA, buffer);
	
	if(condition > 0)
		return true;
	else
		return false;
}

bool operator > (const std::string & op1, const Dine & op2)
{
	const char * buffer = op1.c_str();
	const char * bufferB = op2.name.c_str();
	int condition = op2.compare(bufferB, buffer);
	
	if(condition > 0)
		return true;
	else
		return false;
}

bool operator >= (const Dine & op1, const Dine & op2)
{
	const char * bufferA = op1.name.c_str();
	const char * bufferB = op2.name.c_str();
	int conditionB = op1.compare(bufferA, bufferB);

	if(conditionB >= 0)
		return true;
	else
		return false;
}

bool operator >= (const Dine & op1, const std::string & op2)
{
	const char * buffer = op2.c_str();
	const char * bufferB = op1.name.c_str();
	int condition = op1.compare(bufferB, buffer);
	
	if(condition >= 0)
		return true;
	else
		return false;
}

bool operator >= (const std::string & op1, const Dine & op2)
{
	const char * bufferA = op1.c_str();
	const char * bufferB = op2.name.c_str();
	int condition = op2.compare(bufferB, bufferA);
	
	if(condition >= 0)
		return true;
	else
		return false;
}

double operator + (const Dine & op1, const int & op2)
{
	double price = 0;
	if((op2 - 1) < 0 || (op2 - 1) >= (int) op1.menu.size() || op1.menu.empty() || op1.prices.empty() || op1.menu.size() != op1.prices.size())
		return price;
	else
	{
		return op1.prices.at(op2 - 1);
	}
}

double operator - (const Dine & op1, const int & op2)
{
	return -1 * (op1 + op2);
}

Dine & Dine :: operator += (const double & op1)
{
	prices.push_back(op1);
	return *this;
}

Dine & Dine :: operator -= (const double & op1)
{
	prices.pop_back();
	prices.push_back(op1);
	return *this;
}

Dine & Dine :: operator += (const std::string & op1)
{
	menu.push_back(op1);
	return *this;
}

Dine & Dine :: operator -= (const std::string & op1)
{
	menu.pop_back();
	menu.push_back(op1);
	return *this;
}

Dine & Dine :: operator -- ()
{
	menu.pop_back();
	prices.pop_back();
	return *this;
}

std::istream & Dine :: load(std::istream & input)
{
	if(input)
	{
		char buffer[MAX_STRING];
		double price;
		int items;

		input.getline(buffer, MAX_STRING, '|');
		name = buffer;

		if(!input.eof())
		{
			input >> items; 
			input.ignore(1, '\n');

			for(int i = 0; i<items && !input.eof(); i++)
			{
				input.getline(buffer, MAX_STRING, '|');
				input >> price;
				input.ignore(1, '\n');
				menu.push_back(buffer);
				prices.push_back(price);
			}
			
		}
	}
	return input;
}




