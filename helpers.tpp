#ifndef HELPERS_TPP
#define HELPERS_TPP

#include <type_traits>

template<class TYPE>
int Menu :: listMenuSelect(DLL<TYPE> & list, char & option)
{
	do
	{
		printListMenu(list);
		option = getChar();
		listMenuRead(list, option);
	}
	while(option != 'q' && option != '<');

	if(option == '<')
		option = 'a';
	return 1;
}

template<class TYPE>
void Menu :: printListMenu(DLL<TYPE> & list) const
{
	if constexpr (std::is_same_v<TYPE, Tour>)
		printTourMenu();
		
	else if constexpr (std::is_same_v<TYPE, Game>)
		printGameMenu();

	else if constexpr (std::is_same_v<TYPE, Dine>)
		printDineMenu();
}

template<class TYPE>
int Menu :: listMenuRead(DLL<TYPE> & list, char & option)
{
	std::ifstream loadFile;
	std::string fileName;

	if constexpr (std::is_same_v<TYPE, Tour>)
		fileName = "tourData.txt";
		
	else if constexpr (std::is_same_v<TYPE, Game>)
		fileName = "gameData.txt";

	else if constexpr (std::is_same_v<TYPE, Dine>)
		fileName = "dineData.txt";
	
	loadFile.open(fileName);
	
	switch(option)
	{
		case'a':
			displayList(list);
			break;
		case 'b':
			insertList(list);
			break;
		case 'c':
			retrieveList(list, option);
			break;
		case 'd':
			removeList(list, option);
			break;
		case 'e':
			removeAllList(list, option);
			break;
		case 'f':
			loadList(list, loadFile, option);
			break;
		case 'g':
			plusCurrent(list);
			break;
		case 'h':
			minusCurrent(list);
			break;
		case 'i':
			plusEqualCurrent(list);
			break;
		case 'j':
			minusEqualCurrent(list);
			break;
		case 'l':
			++list;
			break;
		case 'm':
			--list;
			break;
		case 'q':
			break;
		case '<':
			break;
		default:
			cout << "Invalid option..." << endl;
	}
	return 1;
}


template<class TYPE> void Menu :: displayList(DLL<TYPE> & list) const
{
	cout << list << endl;
}

template<class TYPE> void Menu :: plusCurrent(DLL<TYPE> & list)
{
	cout << "Enter nodes to travel (+): ";
	int num = getInt();
	Node<TYPE> * current = list + num;
	if(current)
		cout << "Peek:" << endl << *current << endl;
	else
		cout << "Current points to nullptr" << endl;
}

template<class TYPE> void Menu :: minusCurrent(DLL<TYPE> & list)
{
	cout << "Enter nodes to travel (-): ";
	int num = getInt();
	Node<TYPE> * current;
	current = list - num;
	if(current)
		cout << "Peek:" << endl << *current << endl;
	else
		cout << "Current points to nullptr" << endl;
}

template<class TYPE> void Menu :: plusEqualCurrent(DLL<TYPE> & list)
{
	cout << "Enter nodes to travel (+=): ";
	int num = getInt();
	list += num;
	cout << list << endl;
}

template<class TYPE> void Menu :: minusEqualCurrent(DLL<TYPE> & list)
{
	cout << "Enter nodes to travel (-=): ";
	int num = getInt();
	list -= num;
	cout << list << endl;
}

template<class TYPE> void Menu :: insertList(DLL<TYPE> & list)
{
	Node<TYPE> * temp = nullptr;

	if constexpr (std::is_same_v<TYPE, Tour>)
		createTour(temp);
		
	else if constexpr (std::is_same_v<TYPE, Game>)
		createGame(temp);

	else if constexpr (std::is_same_v<TYPE, Dine>)
		createDine(temp);
	
	list.insert(temp);
}

template<class TYPE> void Menu :: retrieveList(DLL<TYPE> & list, char & option)
{
	cout << "Enter entry index: ";
	int index = getInt();
	Node<TYPE> * node = nullptr;
	list.retrieve(index, node);

	if(node)
	{
		if constexpr (std::is_same_v<TYPE, Tour>)
			tourManipSelect(node, option);

		else if constexpr (std::is_same_v<TYPE, Game>)
			gameManipSelect(node, option);

		else if constexpr (std::is_same_v<TYPE, Dine>)
			dineManipSelect(node, option);
	}
}
		
template<class TYPE> void Menu :: removeList(DLL<TYPE> & list, char & option)
{
	cout << "Enter index to remove: ";
	int index = getInt();
	list.remove(index);
}

template<class TYPE> void Menu :: removeAllList(DLL<TYPE> & list, char & option)
{
	cout << "Remove all items from list? (y/n): ";
	char choice;
	if(yesNo(choice))
		list.removeAll();
}

template<class TYPE> void Menu :: loadList(DLL<TYPE> & list, istream & input, char & option)
{
	cout << "Loading will override changes. Confirm? (y/n): ";
	if(yesNo(option))
	{
		if constexpr (std::is_same_v<TYPE, Tour>)
			loadTours(input);
	
		else if constexpr (std::is_same_v<TYPE, Game>)
			loadGames(input);

		else if constexpr (std::is_same_v<TYPE, Dine>)
			loadDines(input);
	}
}

template<class TYPE> void Menu :: updateAddress(Node<TYPE> * & node, char & option)
{
	TYPE temp;
	temp << *node;
	cout << "Enter new address: ";
	cin >> static_cast<Activity &>(temp);
	*node = temp;
}

template<class TYPE> void Menu :: updateTime(Node<TYPE> * &  node, char & option)
{
	TYPE temp;
	temp << *node;
	cout << "Enter new Time: ";
	int num = getInt();
	static_cast<Activity &>(temp) += num;
	*node = temp;	
}

template<class TYPE> void Menu :: updateText(Node<TYPE> *  & node, char & option)
{
	TYPE temp;
	temp << *node;
	cout << "Enter new text: ";
	cin >> temp;
	*node = temp;
}

template <class TYPE>
void Menu :: testEqualities(Node<TYPE> * & node)
{
	if(node)
	{
		TYPE temp;
		temp << *node;
		cout << temp << endl;
		cout << "Enter string to compare: ";
		string buffer = getText();
		isEqual(temp, buffer);
		isNotEqual(temp, buffer);
		lessThan(temp, buffer);
		lessThanEqual(temp, buffer);
		greatThan(temp, buffer);
		greatThanEqual(temp, buffer);
	}
}

template <class TYPE> void Menu :: isEqual(TYPE op1, TYPE op2)
{
	cout << "Testing == ... ";
	bool test = op1 == op2;
	if(test)
		cout << "True, " << op1 << " == " << op2 << endl;
	else
		cout << "False, " << op1 << " != " << op2 << endl;
}

template <class TYPEA, class TYPEB> void Menu :: isEqual(TYPEA op1, TYPEB op2)
{
	cout << "Testing == ... ";
	bool test = op1 == op2;
	if(test)
		cout << "True, " << op1 << " == " << op2 << endl;
	else
		cout << "False, " << op1 << " != " << op2 << endl;
}

template <class TYPE> void Menu :: isNotEqual(TYPE op1, TYPE op2)
{
	cout << "Testing != ... ";
	bool test = op1 != op2;
	if(test)
		cout << "True, " << op1 << " != " << op2 << endl;
	else
		cout << "False, " << op1 << " == " << op2 << endl;
}

template <class TYPEA, class TYPEB> void Menu :: isNotEqual(TYPEA op1, TYPEB op2)
{
	bool test = op1 != op2;
	if(test)
		cout << "True, " << op1 << " != " << op2 << endl;
	else
		cout << "False, " << op1 << " == " << op2 << endl;
}

template <class TYPE> void Menu :: lessThan(TYPE op1, TYPE op2)
{
	bool test = op1 < op2;
	if(test)
		cout << "True, " << op1 << " < " << op2 << endl;
	else
		cout << "False, " << op1 << " !< " << op2 << endl;
}

template <class TYPEA, class TYPEB> void Menu :: lessThan(TYPEA op1, TYPEB op2)
{
	bool test = op1 < op2;
	if(test)
		cout << "True, " << op1 << " < " << op2 << endl;
	else
		cout << "False, " << op1 << " !< " << op2 << endl;
}

template <class TYPE> void Menu :: lessThanEqual(TYPE op1, TYPE op2)
{
	bool test = op1 < op2;
	if(test)
		cout << "True, " << op1 << " <= " << op2 << endl;
	else
		cout << "False, " << op1 << " !<= " << op2 << endl;
}

template <class TYPEA, class TYPEB> void Menu :: lessThanEqual(TYPEA op1, TYPEB op2)
{
	bool test = op1 < op2;
	if(test)
		cout << "True, " << op1 << " <= " << op2 << endl;
	else
		cout << "False, " << op1 << " !<= " << op2 << endl;
}

template <class TYPE> void Menu :: greatThan(TYPE op1, TYPE op2)
{
	bool test = op1 < op2;
	if(test)
		cout << "True, " << op1 << " < " << op2 << endl;
	else
		cout << "False, " << op1 << " !< " << op2 << endl;
}

template <class TYPEA, class TYPEB> void Menu :: greatThan(TYPEA op1, TYPEB op2)
{
	bool test = op1 < op2;
	if(test)
		cout << "True, " << op1 << " < " << op2 << endl;
	else
		cout << "False, " << op1 << " !< " << op2 << endl;
}

template <class TYPE> void Menu :: greatThanEqual(TYPE op1, TYPE op2)
{
	bool test = op1 < op2;
	if(test)
		cout << "True, " << op1 << " <= " << op2 << endl;
	else
		cout << "False, " << op1 << " !<= " << op2 << endl;
}

template <class TYPEA, class TYPEB> void Menu :: greatThanEqual(TYPEA op1, TYPEB op2)
{
	bool test = op1 < op2;
	if(test)
		cout << "True, " << op1 << " <= " << op2 << endl;
	else
		cout << "False, " << op1 << " !<= " << op2 << endl;
}

#endif





